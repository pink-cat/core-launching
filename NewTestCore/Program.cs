﻿using CoreLaunching;
using CoreLaunching.Compatibility;
using CoreLaunching.DownloadAPIs;
using CoreLaunching.DownloadAPIs.Forge;
using CoreLaunching.DownloadAPIs.Modrinth;
using CoreLaunching.DownloadAPIs.Universal;
using CoreLaunching.Installers;
using CoreLaunching.JsonTemplates;
using Newtonsoft.Json;

namespace NewTestCore
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var a = VersionsManager.GetOptiFineFiles().Where(x => x.Version == "1.10.2");
            var b = a.First();
            var vb = VersionJsonUtil.Parse(
                CoreLaunching.PinKcatDownloader.ParseType.NativeUrl, 
                VersionsManager.GetVersions()
                .Where(x => x.Id == b.Version).First().Url);
            var tsk = b.InstallOptiFine("J:\\CLTests", 
                jREPath: JavaInfoUtil.GetJavaInfosInLogicalDrives()
                .Where(x => x.MajorVersion >= vb.JavaVersion.MajorVersion).First().Path);
            tsk.InfoOutput += Tsk_InfoOutput;
            while (true)
            {
                Thread.Sleep(1000);
                if (tsk.InstallerStatu == CoreLaunching.DownloadAPIs.Interfaces.InstallerStatuEnum.Installed)
                {
                    //vb.Id = ;
                    break;
                }
            }
        }

        private static void Tsk_InfoOutput(object? sender, string e)
        {
            Console.WriteLine(e);
        }

        private static void Inst_Output(object? sender, string e)
        {
            Console.WriteLine(e);
        }
    } 
}
