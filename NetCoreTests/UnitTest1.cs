using CoreLaunching.DownloadAPIs.Universal;
using CoreLaunching.Installers;
using CoreLaunching.JsonTemplates;

namespace NetCoreTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void InstallNormalVersion()
        {
            var wev = VersionsManager.GetVersions(DownloadSources.BMCLAPI_Version_Manifest).First((x => x.Type == "release"));
            var ma = wev.Install("J:\\CLTests");
            ma.Result.DownloadedSizeUpdated += (x, y) =>
            {
                Console.WriteLine($"delta: {y}, Now: {x.DownloadedSize}, Total:{x.TotalSize}");
            };
            while (true)
            {
                Thread.Sleep(1000);
            }
        }

        [TestMethod]
        public void RepaireNormalVersion()
        {
            var ver = VersionJsonUtil.Parse(CoreLaunching.PinKcatDownloader.ParseType.FilePath, "J:\\CLTests\\versions\\1.20.4\\1.20.4.json");
            var ma = ver.Repaire("J:\\CLTests", DownloadSources.GetMojangAPI());

            ma.DownloadedSizeUpdated += (z, y) =>
            {
                Console.WriteLine($"delta: {y}, Now: {ma.BytesReceived}, Total:{ma.TotalBytes}");
            };
            while (true)
            {
                Thread.Sleep(1000);
            }
        }
        [TestMethod]
        public void InstallOptiFine()
        {
            var a = VersionsManager.GetOptiFineFiles().Where(x => x.Version == "1.10.2");
            var b = a.First();
            var vb = VersionJsonUtil.Parse(CoreLaunching.PinKcatDownloader.ParseType.NativeUrl, VersionsManager.GetVersions().Where(x => x.Id == b.Version).First().Url);
            var tsk = b.InstallOptiFine("J:\\CLTests", jREPath: JavaInfoUtil.GetJavaInfosInLogicalDrives().Where(x => x.MajorVersion >= vb.JavaVersion.MajorVersion).First().Path);
            //tsk.InfoOutput += Tsk_InfoOutput;
            while (true)
            {
                Thread.Sleep(1000);
                if (tsk.InstallerStatu == CoreLaunching.DownloadAPIs.Interfaces.InstallerStatuEnum.Installed)
                {
                    break;
                }
            }
        }
    }
}