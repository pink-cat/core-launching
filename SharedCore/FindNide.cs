﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreLaunching
{
    public static class FindNide
    {
        private const string filename = "nide";
        private static List<string> keywords = new List<string>() {"nide","minecraft","运行", "库", "组件", };
        public static bool GetPath(out string path)
        {
            path = string.Empty;
            var rot = new DirectoryInfo(Path.GetDirectoryName(System.Environment.CurrentDirectory));
            var dics = new List<DirectoryInfo>() { rot };
            foreach (var item in rot.GetDirectories())
            {
                dics.Add(item);
            }
            for(int i =0;i< dics.Count; i++)
            {
                var dic = dics[i];
                try
                {
                    foreach (var subd in dic.EnumerateDirectories())
                    {
                        foreach (var kw in keywords)
                        {
                            if (subd.Name.ToLower().Contains(kw))
                            {
                                dics.Add(subd);
                            }
                        }
                    }
                }
                catch
                {
                    continue;
                }
            }
            for (int i = 0; i < dics.Count; i++)
            {
                try
                {
                    foreach (var item in dics[i].EnumerateFiles())
                    {
                        if (item.Name.EndsWith(".jar"))
                        {
                            if (item.Name.ToLower().Contains(filename))
                            {
                                path = item.FullName;
                                return true;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    continue;
                }
            }
            return false;
        }
    }
}
