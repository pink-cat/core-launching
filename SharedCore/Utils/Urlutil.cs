﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;

namespace CoreLaunching
{
    public static class LinkUtil
    {
        public static void GoTo(string url)
        {
            try
            {
                // 使用Process.Start方法打开网址
                ProcessStartInfo psi = new ProcessStartInfo
                {
                    FileName = url,
                    UseShellExecute = true
                };
                Process.Start(psi);
            }
            catch (Exception ex)
            {
                Console.WriteLine("无法打开网址：" + ex.Message);
            }
        }
        static char _sp='0'; 
        public static char GetSeparator()
        {
            if (_sp== '0')
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    _sp = '\\';
                }
                else { _sp = '/'; }
            }
            return _sp;
        }

        public static string Redirect(string url) 
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "HEAD";
            req.AllowAutoRedirect = false;
            using (WebResponse response = req.GetResponse())
            {
                url = response.Headers["Location"];
            }
            return url;

        }

//        public static Stream GetStream(string url)
//        {
//            url = Redirect(url);
//#if __XAMARIN_ANDROID_v1_0__

//#else

//#endif
//        }
    }
}
