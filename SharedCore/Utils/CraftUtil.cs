﻿#if ANDROID
using Android.Util;
using CoreLaunching.Accounts;
using CoreLaunching.JsonTemplates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Android.OS.Build;

namespace CoreLaunching.Utils
{
    public class CraftUtil
    {
        public static void DisableSplash(string dir)
        {
            DirectoryInfo configDir = new(Path.Combine(dir, "config"));
            if (configDir.Exists || configDir.TryCreate())
            {
                FileInfo forgeSplashFile = new(Path.Combine(dir, "config/splash.properties"));
                string forgeSplashContent = "enabled=true";
                try
                {
                    if (forgeSplashFile.Exists)
                    {
                        forgeSplashContent = File.ReadAllText(forgeSplashFile.FullName);
                    }
                    if (forgeSplashContent.Contains("enabled=true"))
                    {
                        File.WriteAllText(forgeSplashFile.FullName,forgeSplashContent.Replace("enabled=true", "enabled=false"));
                    }
                }
                catch (IOException e)
                {
                    //Log.w(Tools.APP_NAME, "Could not disable Forge 1.12.2 and below splash screen!", e);
                }
            }
            else
            {
                //Log.w(Tools.APP_NAME, "Failed to create the configuration directory");
            }
        }

        #region 未完成
        //public static void LaunchMinecraft(Activity activity, IAccount minecraftAccount,
        //                           Root minecraftProfile, int memories, GameInfo game, JavaInfo runtime, Launcher laun)
        //{
        //    //int freeDeviceMemory = MemoryUtil.GetFree(activity);
        //    //Runtime runtime = MultiRTUtils.forceReread(Tools.pickRuntime(minecraftProfile, versionJavaRequirement));
        //    //JMinecraftVersionList.Version versionInfo = Tools.getVersionInfo(versionId);
        //    //LauncherProfiles.update();
        //    //File gamedir = Tools.getGameDirPath(minecraftProfile);


        //    // Pre-process specific files
        //    DisableSplash(game.GameDir);
        //    string[] launchArgs = StringListUtil.StringToStringList(minecraftProfile.GetArgs(minecraftAccount, laun, game)).ToArray();

        //    // Select the appropriate openGL version
        //    OldVersionsUtils.selectOpenGlVersion(versionInfo);


        //    String launchClassPath = generateLaunchClassPath(versionInfo, versionId);

        //    List<String> javaArgList = new ArrayList<>();

        //    getCacioJavaArgs(javaArgList, runtime.javaVersion == 8);

        //    if (versionInfo.logging != null)
        //    {
        //        String configFile = Tools.DIR_DATA + "/security/" + versionInfo.logging.client.file.id.replace("client", "log4j-rce-patch");
        //        if (!new File(configFile).exists())
        //        {
        //            configFile = Tools.DIR_GAME_NEW + "/" + versionInfo.logging.client.file.id;
        //        }
        //        javaArgList.add("-Dlog4j.configurationFile=" + configFile);
        //    }
        //    javaArgList.addAll(Arrays.asList(getMinecraftJVMArgs(versionId, gamedir)));
        //    javaArgList.add("-cp");
        //    javaArgList.add(getLWJGL3ClassPath() + ":" + launchClassPath);

        //    javaArgList.add(versionInfo.mainClass);
        //    javaArgList.addAll(Arrays.asList(launchArgs));
        //    // ctx.appendlnToLog("full args: "+javaArgList.toString());
        //    String args = LauncherPreferences.PREF_CUSTOM_JAVA_ARGS;
        //    if (Tools.isValidString(minecraftProfile.javaArgs)) args = minecraftProfile.javaArgs;
        //    FFmpegPlugin.discover(activity);
        //    JREUtils.launchJavaVM(activity, runtime, gamedir, javaArgList, args);
        //}
        #endregion
        public static void LaunchMinecraft(Activity activity, IAccount minecraftAccount,
                                   Root minecraftProfile, int memories, GameInfo game, JavaInfo runtime,Launcher laun)
        {
            //int freeDeviceMemory = MemoryUtil.GetFree(activity);
            //Runtime runtime = MultiRTUtils.forceReread(Tools.pickRuntime(minecraftProfile, versionJavaRequirement));
            //JMinecraftVersionList.Version versionInfo = Tools.getVersionInfo(versionId);
            //LauncherProfiles.update();
            //File gamedir = Tools.getGameDirPath(minecraftProfile);


            // Pre-process specific files
            DisableSplash(game.GameDir);
            string[] launchArgs = StringListUtil.StringToStringList(minecraftProfile.GetArgs(minecraftAccount,laun,game)).ToArray();

            // Select the appropriate openGL version
            OldVersionsUtils.selectOpenGlVersion(versionInfo);


            String launchClassPath = generateLaunchClassPath(versionInfo, versionId);

            List<String> javaArgList = new ArrayList<>();

            getCacioJavaArgs(javaArgList, runtime.javaVersion == 8);

            if (versionInfo.logging != null) {
                String configFile = Tools.DIR_DATA + "/security/" + versionInfo.logging.client.file.id.replace("client", "log4j-rce-patch");
                if (!new File(configFile).exists()) {
                    configFile = Tools.DIR_GAME_NEW + "/" + versionInfo.logging.client.file.id;
                }
                javaArgList.add("-Dlog4j.configurationFile=" + configFile);
            }
            javaArgList.addAll(Arrays.asList(getMinecraftJVMArgs(versionId, gamedir)));
            javaArgList.add("-cp");
            javaArgList.add(getLWJGL3ClassPath() + ":" + launchClassPath);

            javaArgList.add(versionInfo.mainClass);
            javaArgList.addAll(Arrays.asList(launchArgs));
            // ctx.appendlnToLog("full args: "+javaArgList.toString());
            String args = LauncherPreferences.PREF_CUSTOM_JAVA_ARGS;
            if (Tools.isValidString(minecraftProfile.javaArgs)) args = minecraftProfile.javaArgs;
            FFmpegPlugin.discover(activity);
            JREUtils.launchJavaVM(activity, runtime, gamedir, javaArgList, args);
        }

    }
}

#endif