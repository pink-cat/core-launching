﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Net.Mime.MediaTypeNames;

namespace CoreLaunching
{
    public static class TextUtil
    {
        public static bool IsOnlyASCII(this string content)
        {
            return OnlyASCII(content);
        }
        public static bool OnlyASCII(string content)
        {
            var res = true;
            for (int i = 0; i < content.Length; i++)
            {
                if ((int)content[i] > 127)
                {
                    res = false;
                    break;
                }
            }
            return res;
        }
    }
}
