﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CoreLaunching
{
    public static class Certutil
    {
        public static bool CompareLength(long size,string filePath)
        {
            if (File.Exists(filePath))
            {
                var fi = new FileInfo(filePath);
                return fi.Length == size;
            }
            else
            {
                return false;
            }
        }

        public static bool CompareSha1(string sha1,string filePath)
        {
            sha1=sha1.ToLower();
            if (File.Exists(filePath))
            {
                return sha1== Sha1(filePath);
            }
            else
            {
                return false;
            }
        }
        public static string Sha1(string filePath)
        {
            if (File.Exists(filePath))
            {   
                using (var fs = File.OpenRead(filePath))
                {

                    var res = Sha1(fs);
                    GC.Collect();
                    return res;
                }
            }
            else
            {
                return "";
            }
        }

        public static string Sha1(Stream stream)
        {
            using (SHA1 sha1 = SHA1.Create())
            {
                stream.Position = 0;
                byte[] resu = sha1.ComputeHash(stream);
                var res = string.Join("", resu.Select(b => string.Format("{0:x2}", b)).ToArray());
                //GC.SuppressFinalize(bytes);
                GC.SuppressFinalize(sha1);
                GC.SuppressFinalize(resu);
                resu = null;
                //bytes = null;
                GC.Collect();
                return res.ToLower();
            }
        }
    }
}
