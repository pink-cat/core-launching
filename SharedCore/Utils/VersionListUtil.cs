﻿using CoreLaunching.JsonTemplates;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net.Http;

namespace CoreLaunching
{
    public static class VersionListUtil
    {
        private static string _sourceLink = "https://launchermeta.mojang.com/mc/game/version_manifest.json";
        public static string SourceLink
        {
            get => _sourceLink; set
            {
                if (value.EndsWith("/"))
                {
                    _sourceLink = value.Substring(0, value.Length - 1);
                }
                else
                {
                    _sourceLink = value;
                }
            }
        }

        private static WebVersionInfo[] _infos = new WebVersionInfo[0];

        public static WebVersionInfo[] Infos
        {
            get
            {
                if (_infos.Length == 0)
                {
                    _infos = GetInfos();
                }
                return _infos;
            }
        }


        private static WebVersionInfo[] GetInfos()
        {
            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Get, _sourceLink))
                {
                    using (var response = client.Send(request))
                    {
                        response.EnsureSuccessStatusCode();
                        using (var stm = response.Content.ReadAsStream())
                        {
                            using (var rd = new StreamReader(stm))
                            {
                                var cont = rd.ReadToEnd();
                                return JsonConvert.DeserializeObject<WebVersionInfo[]>(JObject.Parse(cont)["versions"].ToString());
                            }
                        }
                    }
                }
            }
        }
    }
}
