﻿using CoreLaunching.PinKcatDownloader;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace CoreLaunching
{
    public static class FileVerifyUtil
    {
        public static bool TryCreate(this FileSystemInfo info)
        {
            try
            {
                if (info.Exists)
                {
                    return true;
                }
                else
                {
                    if(info is DirectoryInfo dir)
                    {
                        dir.Create();
                    }
                    else if(info is FileInfo file)
                    {
                        file.Create().Close();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static Version GetFileVersion(string file)
        {
            var version = System.Diagnostics.FileVersionInfo.GetVersionInfo(file);
            return Version.Parse(version.FileVersion);
        }
        public static MCFileInfo[] Verify(this IEnumerable<MCFileInfo> sources, IEnumerable<MCFileInfo> mustRemove =null)
        {
            var result = sources.ToList();
            if (mustRemove!=null)
            {
                foreach (var item in mustRemove)
                {
                    if (result.Contains(item))
                    {
                        result.Remove(item);
                    }
                }
            }
            foreach (var source in sources) {
                if (!Certutil.CompareLength(source.Size, source.Local))
                {
                        //result.Add(source);
                }
                else
                {
                    var sha1 = Certutil.Sha1(source.Local);
                    if( sha1 != source.Sha1)
                    {
                            //result.Add(source);
                    }
                    else
                    {
                        result.Remove(source);
                    }
                }
            }
            return result.Distinct(new InfoCompare()).ToArray();
        }

        public static char GetSeparator()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return '\\';
            }
            else { return '/'; }
        }
    }
}
