﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Environment = System.Environment;
#if ANDROID
using Android.OS;
#endif

namespace CoreLaunching
{
    public class JavaInfo
    {
        public bool Is64Bit { get; set; }
        public string Path { get; set; }
        public int MajorVersion { get; set; }
        public static bool TryParse(string path, out JavaInfo info)
        {
#if ANDROID
            try
            {
                info = new() { Path=path};
                using (var strm = File.OpenText(path))
                {
                    var txt = strm.ReadLine();
                    while (!string.IsNullOrEmpty(txt))
                    {
                        if (txt.Contains("JAVA_VERSION="))
                        {
                            txt = txt.Replace("JAVA_VERSION=", "").Replace("\"", "");
                            if (txt.StartsWith("1."))
                            {
                                info.MajorVersion = Convert.ToInt32(txt.Replace("1.", "")[0].ToString()); 
                                txt = strm.ReadLine();
                                continue;
                            }
                            var mjor = "";
                            for (int i = 0; i < txt.Length; i++)
                            {
                                if (((int)txt[i]) >=48&& ((int)txt[i]) <= 57)
                                {
                                    mjor += txt[i];
                                }
                                else
                                {
                                    break;
                                }
                            }
                            info.MajorVersion= Convert.ToInt32(mjor); 
                            txt = strm.ReadLine();
                            continue;
                        }
                        else if (txt.Contains("OS_ARCH="))
                        {
                            if(txt.Replace("OS_ARCH=","").Replace("\"","")== "aarch64")
                            {
                                info.Is64Bit = true;
                            }
                            else
                            {
                                info.Is64Bit = false;
                            }
                            txt = strm.ReadLine();
                            continue;
                        }
                        txt = strm.ReadLine();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                info = null;
                return false;
            }
#else

            try
            {
                info = new JavaInfo();
                info.Path = path;
                var version = FileVerifyUtil.GetFileVersion(path);
                info.Is64Bit = false;
                using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader binaryReader = new BinaryReader(fileStream))
                    {
                        if (binaryReader.ReadUInt16() == 23117)
                        {
                            fileStream.Seek(58L, SeekOrigin.Current);
                            fileStream.Seek(binaryReader.ReadUInt32(), SeekOrigin.Begin);
                            if (binaryReader.ReadUInt32() == 17744)
                            {
                                fileStream.Seek(20L, SeekOrigin.Current);
                                var num2 = binaryReader.ReadUInt16();
                                info.Is64Bit = num2 == 523 || num2 == 267;
                            }
                        }
                    }
                }
                info.MajorVersion = version.Major;
                return true;
            }
            catch (Exception ex)
            {
                info = null;
                return false;
            }
#endif
        }

        public override string ToString()
        {
            return $"{Path}:{MajorVersion}";
        }
    }
    public static class JavaInfoUtil
    {
#if ANDROID
        public static IEnumerable<JavaInfo> GetJavaInfosInFolder(string folder)
#else
        public static IEnumerable<JavaInfo> GetJavaInfosInLogicalDrives()
#endif
        {
            var res = new List<JavaInfo>();
#if ANDROID
            foreach (var item in GetFileInfosInFolder(folder))
#else
            foreach (var item in GetFileInfosInLogicalDrives())
#endif
            {
                if (JavaInfo.TryParse(item.FullName, out var info))
                {
                    res.Add(info);
                }
            }
            return res;
        }
        public static List<string> HighFrequencyKeyWords = new() {
            "java", "jdk", "jbr", "bin",
#if ANDROID
            "android","download","com"
#else
            "game","游戏","运行库",
            "minecraft","mc","世界","创世神",
            "program files","software",
            "oracle", "eclipse", "microsoft","hotspot", "idea", "android",
            "code","language",
            "语言","代码"
#endif
            };
#if ANDROID
        internal static IEnumerable<FileInfo> GetFileInfosInFolder(string folder)
#else
        internal static IEnumerable<FileInfo> GetFileInfosInLogicalDrives()
#endif
        {
            var res = new List<FileInfo>();
            var dirs = new List<DirectoryInfo>();
#if ANDROID
            dirs.Add(new DirectoryInfo(folder));
#else
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                foreach (var drv in Environment.GetLogicalDrives())
                {
                    dirs.Add(new(drv));
                }
                dirs.Add(new(Path.GetDirectoryName(System.Environment.CurrentDirectory)));
            }
            else
            {
                throw new NotSupportedException();
            }
#endif
            for (int i = 0; i < dirs.Count; i++)
            {
                var dir = dirs[i];
                try
                {
                    foreach (var item in dir.EnumerateDirectories())
                    {
                        foreach (var keyword in HighFrequencyKeyWords)
                        {
                            if (item.Name.ToLower().Contains(keyword))
                            {
                                dirs.Add(item);
                                break;
                            }
                        }
                    }
                }
                catch
                {

                }
            }
            foreach (var item in dirs)
            {
#if ANDROID
                var info = new FileInfo(Path.Combine(item.FullName, "release"));
                if (info.Exists)
                {
                    res.Add(info);
                }
#else
                var info = new FileInfo(Path.Combine(item.FullName, "java.exe"));
                var info2 = new FileInfo(Path.Combine(item.FullName, "javaw.exe"));
                if (info.Exists)
                {
                    res.Add(info);
                }
                if (info2.Exists)
                {
                    res.Add(info2);
                }
#endif
            }
            return res;
        }
    }
}
