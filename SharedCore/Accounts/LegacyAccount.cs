using CoreLaunching.Accounts;

namespace CoreLaunching;

public class LegacyAccount : IAccount
{
    public string UserName { get; set; }
    public string Uuid { get; set; }
    public string AccessToken { get; set; }
    public string UserProperties { get; set; }
    public string UserType { get =>"Legacy"; set { } }
    public string ClientID { get; set; }
    public string Xuid { get; set; }
    public string JVMArgs { get; set; }

    public bool SingUp(out string errorInfo)
    {
        errorInfo = string.Empty;
        return true;
    }

    public LegacyAccount(string name):this(name,Guid.NewGuid().ToString())
    {

    }

    public LegacyAccount(string name, string guid)
    {
        if (Guid.TryParse(guid, out var guidValue))
        {
            this.UserName = name;
            this.AccessToken = guid;
        }
        else
        {
            throw new InvalidOperationException($"{guid} 不是合法的 GUID! ");
        }
        AccessToken = "1234567890";
        UserProperties = "{}";
        ClientID = "${clientid}";
        Xuid = "${auth_xuid}";
    }
}