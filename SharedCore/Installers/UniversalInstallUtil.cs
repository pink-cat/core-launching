using CoreLaunching.DownloadAPIs;
using CoreLaunching.DownloadAPIs.Forge;
using CoreLaunching.DownloadAPIs.Universal;
using CoreLaunching.Forge;
using CoreLaunching.JsonTemplates;
using CoreLaunching.PinKcatDownloader;
using Newtonsoft.Json;
using System;
using System.Net;

namespace CoreLaunching.Installers
{
    /// <summary>
    /// 注：不会自动下载资源文件。
    /// </summary>
    public static class UniversalTool
    {
        public static DownloaderWithProgress Repaire(this IVersionJson version, string dotMCPath,Dictionary<string,string> sources = null,string tempFolder = null)
        {
            tempFolder = tempFolder ?? System.IO.Path.GetTempPath();
            sources = sources ?? DownloadSources.GetBmclapiAPI();
            var lst = version.GetFiles(dotMCPath, false,sources).ToList();
            for (int i = 0; i < lst.Count; i++)
            {
                if (File.Exists(lst[i].Local))
                {
                    lst.RemoveAt(i);
                    i--;
                }
            }
            var pm = new DownloaderWithProgress(lst);
            pm.Start();
            return pm;
        }
        public static ForgeInstaller InstallForge(this IVersionJson version,IForgeVersionInfo forge,string dotMCPath,string name = "", Dictionary<string, string> sources = null, string tempFolder = null)
        {
            var ins = new ForgeInstaller();
            //ins.InstallClientAsync();
            return ins;
        }

        public static ForgeInstaller InstallForge(this WebVersionInfo version, IForgeVersionInfo forge, string dotMCPath, string name = "", Dictionary<string, string> sources = null, string tempFolder = null)
        {
            var ins = new ForgeInstaller();
            return ins;
        }
        public static NewForgeInstaller OneKeyInstall(this IForgeVersionInfo forge, string dotMCPath, string gamename = null, Dictionary<string, string> sources = null, string tempFolder = null, string jREPath = null)
        {
            var om = VersionsManager.GetVersions().Where(x=>x.Id==forge.GameVersionString).First();
            gamename = gamename ?? $"{om.Id}-{forge.Name}";
            var ins = new NewForgeInstaller(jREPath, dotMCPath, gamename,om,forge);
            ins.InstallAsync();
            return ins;
        }

        public static OptiFineInstaller InstallOptiFine(this OptifineFileInfo opti, string dotMCPath, string gamename = null, Dictionary<string, string> sources = null, string tempFolder = null, string jREPath = null)
        {
            var ifr = VersionsManager.GetVersions().Where(x => x.Id == opti.Version).First();
            var ins = new OptiFineInstaller(JavaInfoUtil.GetFileInfosInLogicalDrives().First().FullName,opti,dotMCPath);
            ins.InstallAsync();
            return ins;
        }
    }
}