﻿using CoreLaunching.DownloadAPIs.Forge;
using CoreLaunching.DownloadAPIs.Interfaces;
using CoreLaunching.DownloadAPIs.Universal;
using CoreLaunching.JsonTemplates;
using CoreLaunching.Parser;
using CoreLaunching.PinKcatDownloader;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CoreLaunching.Forge
{
    public class NewForgeInstaller : IModLoaderInstaller
    {
        public Dictionary<string,string> Sources { get; set; }
        public IForgeVersionInfo ForgeInfo { get; set; }
        public string JREPath { get; set; }
        public string DotMCPath { get; set; }
        public string GameName { get; set; }
        public string LocalInstaller { get; set; } = Path.Combine(Path.GetTempPath(),Path.GetRandomFileName());
        public string BaseInfo { get; set; }
        private string _MinecraftJarPath;
        public InstallerStatuEnum InstallerStatu { get ; set ; }
        ParseType BaseInfoType { get; set; }
        public int CurrectIndex { get ; set ; }
        public IEnumerable<string> Infos { get ; set ; } = new string[8] { "", "", "", "", "", "", "", "" };
        public double Progress { get ; set ; }
        public int BytesPerSec { get; set; }
        public long BytesReceived { get; set; }
        public long TotalBytes { get; set; }

        public NewForgeInstaller(string jREPath, string dotMCPath, string gameName, WebVersionInfo info,IForgeVersionInfo forge, Dictionary<string, string> sources = null) :this(jREPath,dotMCPath,gameName,"",forge,ParseType.NativeUrl,sources)
        {
            BaseInfoType = ParseType.NativeUrl;
            BaseInfo = info.Url;
            ForgeInfo=forge;
        }

        public NewForgeInstaller(string jREPath, string dotMCPath, string gameName, string baseInfo, IForgeVersionInfo forge, ParseType pt = ParseType.FilePath,Dictionary<string,string> sources = null)
        {
            Sources = sources ?? DownloadSources.GetBmclapiAPI();
            JREPath = jREPath;
            DotMCPath = dotMCPath;
            GameName = gameName;
            BaseInfo = baseInfo;
            BaseInfoType = pt;
            ForgeInfo = forge;
            InstallerStatu = InstallerStatuEnum.NotExecuted;
            _MinecraftJarPath = dotMCPath + LinkUtil.GetSeparator() + "version" + LinkUtil.GetSeparator() + $"{gameName}.jar";
        }
        public NewForgeInstaller(string jREPath, string dotMCPath, string gameName,IForgeVersionInfo forge, bool update=true, Dictionary<string, string> sources = null) :this(jREPath,dotMCPath,gameName,"",forge,ParseType.Json,sources)
        {
            BaseInfo = File.ReadAllText(dotMCPath+LinkUtil.GetSeparator()+"version"+LinkUtil.GetSeparator()+$"{gameName}.json");
            _MinecraftJarPath = dotMCPath + LinkUtil.GetSeparator() + "version" + LinkUtil.GetSeparator() + $"{gameName}.jar";
        }
        List<FileSystemInfo> _caches = new();
        public bool Disposed;
        public IEnumerable<FileSystemInfo> Caches() => _caches;

        public void Install()
        {
            using (var clt = new WebClient())
            {
                var ur = ForgeInfo.GetInstallJarUrl();
                clt.DownloadFile(ur, LocalInstaller);
            }
            InstallerStatu = InstallerStatuEnum.Running;
            var fi = new DirectoryInfo(Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName())));
            fi.Create();
            ZipFile.ExportAll(LocalInstaller,fi.FullName);
            _caches.Add(fi);

            var librariesPath = Path.Combine(DotMCPath,"libraries");
            var workPath = fi.FullName;
            Directory.CreateDirectory(workPath);

            if (BaseInfoType == ParseType.FilePath)
            {
                BaseInfo = File.ReadAllText(BaseInfo);
            }
            else if (BaseInfoType == ParseType.NativeUrl)
            {
                using (var clt = new WebClient())
                {
                    BaseInfo = clt.DownloadString(BaseInfo);
                }
            }
            try
            {
                var insp = JsonConvert.DeserializeObject<InstallProfile>(System.IO.File.ReadAllText(Path.Combine(workPath, "install_profile.json")));

                var mCr = VersionJsonUtil.Parse<VersionJson>(ParseType.Json, BaseInfo);
                var fCr = VersionJsonUtil.Parse<VersionJson>(ParseType.Json, System.IO.File.ReadAllText(Path.Combine(workPath, "version.json")));
                mCr.MainClass = fCr.MainClass;
                mCr.Id = GameName;
                var datab = insp.Data;

                mCr.Arguments.Jvm.CombineArguments(fCr.Arguments.Jvm);
                mCr.Arguments.Game.CombineArguments(fCr.Arguments.Game);
                foreach (var item in fCr.Libraries)
                {
                    if (mCr.Libraries.Where(x => x.Name == item.Name).Count() == 0)
                    {
                        mCr.Libraries.Add(item);
                    }
                }

                var nP = datab["{MOJMAPS}"].Client;
                nP = nP.Replace("[", "").Replace("]", "");
                nP = nP.GetLibraryFileName(Path.Combine(DotMCPath, "libraries"));


                if (string.IsNullOrEmpty(JREPath))
                {
                    var lq = JavaInfoUtil.GetJavaInfosInLogicalDrives().Where(x => x.MajorVersion == mCr.JavaVersion.MajorVersion&&x.Path.EndsWith("java.exe"));
                    if (lq != null && lq.Count() > 0)
                    {
                        JREPath = lq.First().Path;
                    }
                }

                    List<MCFileInfo> required = new();
                _MinecraftJarPath = Path.Combine(DotMCPath, "versions", $"{mCr.Id}", $"{mCr.Id}.jar");
                required.AddRange(mCr.GetLibriesUrls(DotMCPath, true, Sources));
                required.AddRange(mCr.Downloads.Client.GetFiles(_MinecraftJarPath, true,Sources));
                required.AddRange(insp.GetFiles(DotMCPath,true, Sources));
                required.Add(new("mappings",mCr.Downloads.Client_mappings.Sha1,mCr.Downloads.Client_mappings.Size,mCr.Downloads.Client_mappings.Url,nP));
                required = required.Where(x=>!File.Exists(x.Local)).ToList();
                var ProcessM = new ProcessManager(required);
                ProcessM.Start(Path.GetTempPath());
                while (!ProcessM.IsFinished)
                {

                }
                if (_MinecraftJarPath.Contains(" "))
                {
                    _MinecraftJarPath = $"\"{_MinecraftJarPath}\"";
                }

                datab.Add("{SIDE}", new ClientAndServerPair("client", "server"));
                datab.Add("{MINECRAFT_JAR}", new ClientAndServerPair(_MinecraftJarPath, string.Empty));
                datab["{BINPATCH}"].Client = "\"" + Path.Combine(workPath, datab["{BINPATCH}"].Client.Remove(0, 1).Replace("/", "\\")) + "\"";
                datab["{BINPATCH}"].Server = "\"" + Path.Combine(workPath, datab["{BINPATCH}"].Server.Remove(0, 1).Replace("/", "\\")) + "\"";


                var procs = insp.Processors.Where((x) => x.IsForClient).ToList();
                for (int i = 0; i < procs.Count; i++)
                {
                    var item = procs[i];
                    var proc = item.GetProcess(JREPath, librariesPath, librariesPath, datab, out var skip);
                    if (skip)
                    {
                        continue;
                    }
                    proc.StartInfo.WorkingDirectory = librariesPath;
                    if (item.Outputs != null)
                    {
                        proc = item.GetProcess(JREPath, librariesPath, librariesPath, datab, out var skipp);
                        if (skipp)
                        {
                            continue;
                        }
                        proc.StartInfo.WorkingDirectory = librariesPath;
                    }
                    using (proc)
                    {
                        proc.StartInfo.RedirectStandardOutput = true;
                        proc.StartInfo.RedirectStandardOutput = true;
                        proc.Start();
                        proc.OutputDataReceived += Proc_OutputDataReceived;
                        proc.BeginOutputReadLine();
                        proc.WaitForExit();
                    }
                }
                File.WriteAllText(_MinecraftJarPath[..^3] +"json",JsonConvert.SerializeObject(mCr));

                InstallerStatu = InstallerStatuEnum.Installed;
            }
            catch (JsonException ex)
            {

            }
        }
        private void Proc_OutputDataReceived(object sender, System.Diagnostics.DataReceivedEventArgs e)
        {
            InfoOutput?.Invoke(this, e.Data);
        }

        public event EventHandler<string> InfoOutput;
        public event PropertyChangingEventHandler? PropertyChanging;

        public Task InstallAsync() => Task.Factory.StartNew(Install);

        protected virtual void Dispose(bool disposing)
        {
            if (!Disposed)
            {
                if (disposing)
                {
                    foreach (var cache in Caches())
                    {
                        if (cache is DirectoryInfo di) di.Delete(true);
                        else cache.Delete();
                    }
                    _caches.Clear();
                }

                Disposed = true;
            }
        }

        // // TODO: 仅当“Dispose(bool disposing)”拥有用于释放未托管资源的代码时才替代终结器
        // ~NewForgeInstaller()
        // {
        //     // 不要更改此代码。请将清理代码放入“Dispose(bool disposing)”方法中
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // 不要更改此代码。请将清理代码放入“Dispose(bool disposing)”方法中
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
    [Obsolete("请使用接口化的NewForgeInstaller")]
    public class ForgeInstaller
    {
        public event EventHandler<string> Output;
        public void InstallClient(string jREPath, string librariesPath, string minecraft_Jar, string mCJson, string installerJar, ParseType type)
        {
            if(minecraft_Jar.Contains(" "))
            {
                minecraft_Jar = $"\"{minecraft_Jar}\"";
            }    
            var workPath = Path.Combine(librariesPath, "[CL]InstallTmp");
            Directory.CreateDirectory(workPath);
            InstallProfile insp;
            Root mCr;
            if (type == ParseType.Json)
            {
                ZipFile.ExportAll(installerJar, workPath);
                insp = JsonConvert.DeserializeObject<InstallProfile>(Path.Combine(workPath, "install_profile.json"));
                mCr = JsonConvert.DeserializeObject<Root>(mCJson);
            }
            else if (type == ParseType.FilePath)
            {
                ZipFile.ExportAll(installerJar, workPath);
                insp = JsonConvert.DeserializeObject<InstallProfile>(System.IO.File.ReadAllText(Path.Combine(workPath, "install_profile.json")));
                mCr = JsonConvert.DeserializeObject<Root>(System.IO.File.ReadAllText(mCJson));
            }
            else
            {
                using (var clt = new WebClient())
                {
                    clt.DownloadFile(installerJar, Path.Combine(workPath, "[CL]FileTmp.jar"));
                    ZipFile.ExportAll(Path.Combine(workPath, "[CL]FileTmp.jar"), workPath);
                    insp = JsonConvert.DeserializeObject<InstallProfile>(System.IO.File.ReadAllText(Path.Combine(workPath, "install_profile.json")));
                    var mCt = clt.DownloadString(mCJson);
                    mCr = JsonConvert.DeserializeObject<Root>(System.IO.File.ReadAllText(mCt));
                }
            }
            var datab = insp.Data;
            datab.Add("{SIDE}", new ClientAndServerPair("client", "server"));
            datab.Add("{MINECRAFT_JAR}", new ClientAndServerPair(minecraft_Jar, string.Empty));
            datab["{BINPATCH}"].Client = "\""+Path.Combine(workPath, datab["{BINPATCH}"].Client.Remove(0, 1).Replace("/", "\\"))+"\"";
            datab["{BINPATCH}"].Server = "\""+Path.Combine(workPath, datab["{BINPATCH}"].Server.Remove(0, 1).Replace("/", "\\"))+"\"";


            var procs = insp.Processors.Where((x) => x.IsForClient).ToList();
            for (int i = 0; i < procs.Count; i++)
            {
                var item = procs[i];
                var proc = item.GetProcess(jREPath, librariesPath, librariesPath, datab,out var skip);
                if (skip)
                {
                    continue;
                }
                proc.StartInfo.WorkingDirectory = librariesPath;
                if (item.Outputs != null)
                {
                    proc = item.GetProcess(jREPath, librariesPath, librariesPath, datab, out var skipp);
                    if (skipp)
                    {
                        continue;
                    }
                    proc.StartInfo.WorkingDirectory = librariesPath;
                }
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.Start();
                proc.OutputDataReceived += Proc_OutputDataReceived;
                proc.BeginOutputReadLine();
                proc.WaitForExit();
            }
            new DirectoryInfo(workPath).Delete(true);
        }
        public Task InstallClientAsync(string jREPath, string librariesPath, string minecraft_Jar, string mCJson, string installerJar, ParseType type)
        {
            return Task.Factory.StartNew(() =>
            {
                InstallClient(jREPath, librariesPath, minecraft_Jar, mCJson, installerJar, type);
            });
        }
        private void Proc_OutputDataReceived(object sender, System.Diagnostics.DataReceivedEventArgs e)
        {
            Output?.Invoke(this,e.Data);
        }
    }
}
