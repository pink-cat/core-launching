﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Text.Json.Serialization;
using CoreLaunching.DownloadAPIs.Interfaces;
using CoreLaunching.DownloadAPIs.Universal;
using CoreLaunching.JsonTemplates;
using Newtonsoft.Json;

namespace CoreLaunching.Installers
{
    public class OptiFineInstaller : IModLoaderInstaller
    {
        private string _inheritedFromJar;
        private readonly string _dotMCPath;
        private readonly string _jREPath;
        private OptifineFileInfo _optiInfo;
        private string _inheritedFromPath;
        private readonly string _customName;
        private string _inheritedFromId;
        public bool IsDisposed;

        public InstallerStatuEnum InstallerStatu { get ; set; }
        public int CurrectIndex { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public IEnumerable<string> Infos { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public double Progress { get; set; }
        public long BytesReceived { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public long TotalBytes { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public event EventHandler<string> InfoOutput;
        public event PropertyChangingEventHandler? PropertyChanging;

        public IEnumerable<FileSystemInfo> Caches() => _caches;

        List<FileSystemInfo> _caches = new();
        public string LocalInstaller { get; set; } = Path.Combine(Path.GetTempPath(), Path.ChangeExtension(Path.GetRandomFileName(),"jar"));
        public void Install()
        {
            InstallerStatu = InstallerStatuEnum.Running;
            var libP = Path.Combine(_dotMCPath, "libraries");
            using (var clt = new WebClient())
            {
                var ur = _optiInfo.GetInstallJarUrl();
                clt.DownloadFile(ur, LocalInstaller);
            }
            Progress = 0.05;

            var launchwrapper = "1.12";
            var zipF = ZipFile.GetSubFileDataStream("launchwrapper-of.txt", LocalInstaller);
            var zipF2 = ZipFile.GetSubFileDataStream("changelog.txt", LocalInstaller);

            VersionJson oldV = null;
            if (string.IsNullOrEmpty(_inheritedFromPath))
            {
                oldV = VersionJsonUtil.Parse(PinKcatDownloader.ParseType.NativeUrl, VersionsManager.GetVersions().Where(x => x.Id == _optiInfo.Version).First().Url);
            }
            else
            {
                oldV = VersionJsonUtil.Parse(PinKcatDownloader.ParseType.FilePath, _inheritedFromPath);
            }
            _inheritedFromId = _inheritedFromId ?? oldV.Id;
            var aPartOfoptiLibName = _customName ?? $"{oldV.Id}_{_optiInfo.Name.Replace("OptiFine_"+_inheritedFromId + "_", "").Replace(".jar", "")}";
            oldV.Id = _customName ?? aPartOfoptiLibName;
            if (string.IsNullOrEmpty(_inheritedFromJar))
            {
                using (var clt = new WebClient())
                {
                    var ur = oldV.Downloads.Client.Url;
                    _inheritedFromJar = Path.Combine(_dotMCPath, $"versions{LinkUtil.GetSeparator()}{oldV.Id}", $"{oldV.Id}.jar");
                    Directory.CreateDirectory(Path.GetDirectoryName(_inheritedFromJar));
                    clt.DownloadFile(ur, _inheritedFromJar);
                }
            }
            _inheritedFromId = _inheritedFromId ?? oldV.Id;
            #region InfoCompleting
            oldV.InheritsFrom = null;
            oldV.Time = DateTime.Now.ToString("s");
            oldV.MainClass = "net.minecraft.launchwrapper.Launch";
            oldV.Arguments.Game.Add("--tweakClass optifine.OptiFineTweaker");
            if (zipF != null)
            {
                using (zipF)
                {
                    using (var r = new StreamReader(zipF))
                    {
                        launchwrapper = r.ReadToEnd() ?? "1.12";
                    }
                }
            }

            var optiLibPath = $"optifine:Optifine:{aPartOfoptiLibName}";
            var wrpL = new LibraryInfo()
            {
                Name = $"net.minecraft:launchwrapper:{launchwrapper}",
                Downloads = new() { Artifact = new Artifact() { } }

            };
            oldV.Libraries.Add(wrpL);
            oldV.Libraries.Add(new()
            {
                Name = optiLibPath
            });
            oldV.Id = Path.GetFileNameWithoutExtension(_inheritedFromJar);
            File.WriteAllText(_inheritedFromJar.Replace(".jar",".json"),JsonConvert.SerializeObject(oldV, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
            #endregion
            #region Installing
            var lw = ZipFile.GetSubFileDataStream($"launchwrapper-of-{launchwrapper}.jar", LocalInstaller);
            if (lw != null)
            {
                using (lw)
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(wrpL.Name.GetLibraryFileName(libP)));
                    using (var fs = File.Create(wrpL.Name.GetLibraryFileName(libP)))
                    {
                        lw.CopyTo(fs);
                    }
                }
            }
            var optl = optiLibPath.GetLibraryFileName(libP);
            Directory.CreateDirectory(Path.GetDirectoryName(optl));
            var p = new Process();
            p.StartInfo = new ProcessStartInfo(_jREPath)
            {
                UseShellExecute = false,
                WorkingDirectory = Path.Combine(_dotMCPath, $"versions{LinkUtil.GetSeparator()}{oldV.Id}"),
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                Arguments = string.Join(" ", new[] {
                    "-cp",
                    LocalInstaller,
                    "optifine.Patcher",
                    _inheritedFromJar,
                    LocalInstaller,
                    optl
                })
                }
                ;
            using (p)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(p.StartInfo.FileName);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(p.StartInfo.Arguments);
                p.StartInfo.RedirectStandardOutput = true;
                p.Start();
                p.OutputDataReceived += P_OutputDataReceived;
                p.BeginOutputReadLine();
                p.WaitForExit();
                this.InstallerStatu = InstallerStatuEnum.Installed;
            }
            #endregion
        }

        private void P_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            InfoOutput?.Invoke(this, e.Data);
        }

        public Task InstallAsync() => Task.Factory.StartNew(Install);

        protected virtual void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    // TODO: 释放托管状态(托管对象)
                }

                // TODO: 释放未托管的资源(未托管的对象)并重写终结器
                // TODO: 将大型字段设置为 null
                IsDisposed = true;
            }
        }

        // // TODO: 仅当“Dispose(bool disposing)”拥有用于释放未托管资源的代码时才替代终结器
        // ~OptiFineInstaller()
        // {
        //     // 不要更改此代码。请将清理代码放入“Dispose(bool disposing)”方法中
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // 不要更改此代码。请将清理代码放入“Dispose(bool disposing)”方法中
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public OptiFineInstaller(string jREPath,OptifineFileInfo optiInfo,string dotMCPath, string customName = null, string inheritedFromPath = null, string inheritedFromId = null,string inheritedFromJar = null)
        {
            _inheritedFromJar = inheritedFromJar;
            _dotMCPath = dotMCPath;
            _jREPath = jREPath;
            _optiInfo = optiInfo;
            _inheritedFromPath = inheritedFromPath;
            _customName = customName;
            _inheritedFromId = inheritedFromId;
        }
    }
}
