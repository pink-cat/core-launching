﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CoreLaunching.PinKcatDownloader
{
    public class ProcessManager
    {
        public static List<MCFileInfo> TaskPool { get; set; } = new();
        public event EventHandler Finished;
        public event EventHandler<MCFileInfo> OneFileFinished;
        public int StepFinished = 0;
        public int DownloadedCount = 0;
        public int TotalCount = 0;
        private long _downloadedSize = 0;
        public long TotalSize { get; set; }
        public long DownloadedSize => _downloadedSize;

        public bool IsFinished { get; set; }

        public event EventHandler<int>? DownloadedCountUpdated;
        public delegate void ProcessManagerDelegate<TEventArgs>(ProcessManager sender, TEventArgs e);
        public event ProcessManagerDelegate<long>? DownloadedSizeUpdated;
        public IEnumerable<MCFileInfo> Files;
        string _temp = string.Empty;
        public ProcessManager(IEnumerable<MCFileInfo> files)
        {
            Files = files.Where(x => TaskPool.Contains(x) == false).ToList();
            foreach (var file in files)
            {
                TotalSize += file.Size;
            }
            TotalCount = Files.Count();

            TaskPool.AddRange(Files);
        }

        SuperSmallProcessManager promss;
        SingleThreadProcessManager proms;
        MutilFileDownloaManager prom;
        public void Start(string temp, bool showLargeDetail = true, OprateError op = OprateError.None)
        {
            Task.Factory.StartNew(() =>
            {
                _temp = temp;
                var superSmall = Files.Where((x) => x.Size < 250000).ToArray();
                var small = Files.Where((x) => x.Size <= 2500000 && x.Size >= 250000).ToArray();
                var large = Files.Where((x) => x.Size > 2500000).ToArray();

                if (superSmall.Length > 0)
                {
                    promss = new SuperSmallProcessManager(superSmall);
                    promss.OneFinished += Proms_OneFinished;
                    promss.OneFailed += Promss_OneFailed;
                    new Thread(() => promss.DownloadSingle(op)).Start();
                }
                else
                {
                    StepFinished++;
                }

                if (small.Length > 0)
                {
                    proms = new SingleThreadProcessManager(small);
                    proms.OneFinished += Proms_OneFinished;
                    proms.OneFailed += Promss_OneFailed;
                    new Thread(() => proms.DownloadSingle(op)).Start();
                }
                else
                {
                    StepFinished++;
                }

                if (large.Length > 0)
                {
                    prom = new MutilFileDownloaManager(large);
                    Directory.CreateDirectory(temp);
                    prom.OneFinished += Prom_OnePartFinsihed;
                    if (showLargeDetail)
                    {
                        prom.OnePartFinsihed += Prom_OnePartFinsihed1;
                    }
                    prom.OneFailed += Promss_OneFailed;
                    new Thread(() => prom.Download(temp, op)).Start();
                }
                else
                {
                    StepFinished++;
                }

                if (StepFinished == 3)
                {
                    IsFinished = true;
                    Finished?.Invoke(this, EventArgs.Empty);
                }

                Task.Factory.StartNew(() =>
                {
                    while (DownloadedCount < TotalCount)
                    {
                        Thread.Sleep(1000);
                    }
                    IsFinished = true;
                    Finished?.Invoke(this, EventArgs.Empty);
                    foreach (var item in large)
                    {
                        var dic = new DirectoryInfo(Path.Combine(temp, Path.GetFileNameWithoutExtension(item.Local)));
                        if (dic.Exists)
                        {
                            dic.Delete(true);
                        }
                    }
                });
            });
        }

        private void Prom_OnePartFinsihed1(object? sender, long e)
        {
            if (sender is MutilFileDownloaManager)
            {
                _downloadedSize += e;
                DownloadedSizeUpdated?.Invoke(this, _downloadedSize);
            }
        }

        private void Prom_OnePartFinsihed(object sender, MCFileInfo e)
        {
            DownloadedCount++;
            DownloadedCountUpdated?.Invoke(sender, DownloadedCount);
            if (sender is MutilFileDownloaManager)
            {
                _downloadedSize += e.Size;
                DownloadedSizeUpdated?.Invoke(this, _downloadedSize);
            }
            OneFileFinished?.Invoke(this, e);
        }

        private void Promss_OneFailed(object? sender, MCFileFailedArgs e)
        {

        }

        private void Proms_OneFinished(object? sender, MCFileInfo e)
        {
            TaskPool.Remove(e);
            DownloadedCount++;

            DownloadedCountUpdated?.Invoke(sender, DownloadedCount);
            if (sender is not MutilFileDownloaManager)
            {
                _downloadedSize += e.Size;
                DownloadedSizeUpdated?.Invoke(this, _downloadedSize);
            }
            OneFileFinished?.Invoke(this, e);
        }

        public void Pause()
        {
            //TODO 暂停吧
        }
    }
    public class SingleThreadProcessManager : ObservableCollection<FileDownloadProgress>
    {
        public MCFileInfo[] Infos { get; set; }
        protected override void InsertItem(int index, FileDownloadProgress item)
        {
            var added = false;
            while (!added)
            {
                try
                {

                    //Console.WriteLine($"Instered {item.GetHashCode()}");
                    //Console.WriteLine(item.ThreadState);
                    if (item.thread.ThreadState == ThreadState.Unstarted)
                    {
                        index = Count;
                        base.InsertItem(index, item);
                        item.thread.Start();
                        added = true;
                    }
                    else
                    {
                        return;
                    }
                }
                catch
                {

                }
            }
        }
        public int MaxNum = 64;
        public event EventHandler QueueEmpty;
        public void DownloadSingle(OprateError op = OprateError.None)
        {
            Queue<MCFileInfo> queue = new();
            foreach (MCFileInfo info in Infos)
            {
                queue.Enqueue(info);
            }
            while (queue.Count > 0)
            {
                while (Count <= MaxNum)
                {
                    if (queue.Count >= 8)
                    {
                        var proc = FileDownloadProgress.CreateSingle(queue.Dequeue(), out var thr, op);
                        proc.Finished += Proc_Finished;
                        Add(proc);
                    }
                    else if (queue.Count < 8 && queue.Count > 0)
                    {
                        var qu = queue.Dequeue();
                        var proc = FileDownloadProgress.CreateSingle(qu, out var thr, op);
                        proc.Finished += Proc_Finished;
                        Add(proc);

                    }
                    else
                    {
                        break;
                    }

                }
                Thread.Sleep(200);
            }
            while (Count > 0)
            {
                Thread.Sleep(100);
            }
            IsQueueEmpty = true;
            QueueEmpty?.Invoke(this, new());
        }
        public event EventHandler<MCFileInfo> OneFinished;
        public event EventHandler<MCFileFailedArgs> OneFailed;
        public long AllLengthGetted { get; set; }
        public bool IsQueueEmpty { get; set; }

        private void Proc_Finished(object? sender, Thread e)
        {
            var data = sender as FileDownloadProgress;
            AllLengthGetted += data.Info.Size;
            Remove(data);
            OneFinished.Invoke(this, data.Info);
        }


        public SingleThreadProcessManager(MCFileInfo[] infos)
        {
            Infos = infos;
        }
    }

    public enum OprateError
    {
        None = 0,
        Skip403 = 1,
        Skip404 = 2
    }
}
