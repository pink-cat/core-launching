﻿using CoreLaunching.DownloadAPIs.Interfaces;
using CoreLaunching.PinKcatDownloader;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CoreLaunching
{
    internal class MCFileInfoDownloading : MCFileInfo
    {
        public string TempFilePath { get; set; }
        public int FinishedParts { get; set; }
        public Queue<WebRequest> WebRequests { get; set; }
        public int TotalParts { get; set; }
        public MCFileInfoDownloading(MCFileInfo info,string tempPath) : base(info.Id, info.Sha1, info.Size, info.Url, info.Local)
        {
            TempFilePath = Path.Combine(tempPath,Path.GetFileNameWithoutExtension(Path.GetRandomFileName()));
            TotalParts = (int)Math.Ceiling((double)info.Size / (double)DownloadingThreadFactory.SizeOfOnePart);
            WebRequests =new Queue<WebRequest>();
        }
    }
    internal static class DownloadingThreadFactory
    {
        public static Random Rand = new Random();
        public static int RunningThreads { get; set; }
        public static int EnabledThreads { get; set; } = 64;
        public static int SizeOfOnePart { get; set; } = 7500000;
        public static bool Break { get; private set; }

        /// <summary>
        /// 创建线程，包括合成方法。
        /// </summary>
        /// <param name="info">文件信息</param>
        /// <returns>线程</returns>
        public static IEnumerable<Thread> GetThread(this MCFileInfo oinfo, IShowProgress downloader, string tempPath)
        {
            var res = new List<Thread>();
            var info = new MCFileInfoDownloading(oinfo,tempPath);
            for (var i = 0; i < info.Size;i += SizeOfOnePart)
            {
                var webrq = HttpWebRequest.CreateHttp(info.Url);
                if (i + SizeOfOnePart > info.Size)
                {
                    webrq.AddRange(i, info.Size);
                }
                else
                {
                    webrq.AddRange(i, i + SizeOfOnePart - 1);
                }
                info.WebRequests.Enqueue(webrq);
            }
            for (int i = 0; i < info.TotalParts; i++)
            {
                var tempNameSt = Path.Combine(tempPath, Path.GetRandomFileName());
                var thr = new Thread(() =>
                {
                    Thread.Sleep(Rand.Next(100,600));
                    var webrq = info.WebRequests.Dequeue();
                    if (webrq == null)
                    {
                        return;
                    }
                    try
                    {
                        using (var resp = webrq.GetResponse())
                        {
                            using (var stream = resp.GetResponseStream())
                            {
                                if (info.TotalParts == 1)
                                {
                                    Directory.CreateDirectory(Path.GetDirectoryName(oinfo.Local));
                                    using (var finalfs = new FileStream(info.Local, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                                    {
                                        stream.CopyTo(finalfs);
                                    }
                                    RunningThreads--;
                                    downloader.BytesReceived += info.Size;
                                    return;
                                }
                                else
                                {
                                    var n = webrq.Headers["range"].Replace("bytes=", "");
                                    var m = "";
                                    for (int j = 0; j < n.Length; j++)
                                    {
                                        if (n[j] == '-')
                                        {
                                            break;
                                        }
                                        m += n[j];
                                    }
                                    var p = System.Convert.ToInt32(m) / SizeOfOnePart;
                                    Directory.CreateDirectory(Path.GetDirectoryName(info.TempFilePath + $"-part{p}.tmp"));
                                    using (var fs = new FileStream(info.TempFilePath + $"-part{p}.tmp", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                                    {
                                        stream.CopyTo(fs);
                                        info.FinishedParts++;
                                        downloader.BytesReceived += (fs.Position+1);
                                    }
                                }
                            }
                            if (webrq.Headers["range"].Contains(info.Size.ToString()))
                            {
                                while (info.FinishedParts < info.TotalParts)
                                {
                                    Thread.Sleep(300);
                                }


                                Directory.CreateDirectory(Path.GetDirectoryName(oinfo.Local));
                                try
                                {
                                    using (var finalfs = File.Create(oinfo.Local))
                                    {
                                        for (int i = 0; i <= info.FinishedParts - 1; i++)
                                        {
                                            using (var tempFs = File.OpenRead(info.TempFilePath + $"-part{i}.tmp"))
                                            {
                                                tempFs.CopyTo(finalfs);
                                            }
                                            new FileInfo(info.TempFilePath + $"-part{i}.tmp").Delete();
                                        }

                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                        }
                    }
                    catch (WebException ex)
                    {
                        DownloadingThreadFactory.Break = true;
                    }
                    RunningThreads--;
                });
                res.Add(thr);
            }
            return res;
        }
    }

    public class DownloaderWithProgress : IShowProgress
    {
        public DownloaderWithProgress()
        {
            
        }
        /// <summary>
        /// 全部的东西。
        /// </summary>
        public IEnumerable<MCFileInfo> Items { get; set; }
        public int CurrectIndex {get;set;}
        public IEnumerable<string> Infos {get;set;}
        public double Progress {get;set;}
        public long BytesReceived { get => bytesReceived; set { bytesReceived = value; DownloadedSizeUpdated?.Invoke(this,bytesReceived); } }
        public long TotalBytes {get;set;}
        public bool IsFinished { get; set; }

        public event EventHandler<long> DownloadedSizeUpdated;

        private Queue<MCFileInfo> _queue = new();
        private long bytesReceived;

        public event PropertyChangingEventHandler? PropertyChanging;

        public DownloaderWithProgress
            (IEnumerable<MCFileInfo> items):this()
        {
            Items = items;
            foreach (var item in Items)
            {
                TotalBytes += item.Size;
                _queue.Enqueue(item);
            }
        }
        public void Download()
        {
            Console.WriteLine(DateTime.Now);
            var tempPath = Path.Combine(Path.GetTempPath(), "CoreLaunching");
            Directory.CreateDirectory(tempPath);
            Start(tempPath);
            while (_queue.Count>0)
            {
                Thread.Sleep(1000);
                //Console.WriteLine($"{BytesReceived} in {TotalBytes}");
            }
            int annouce = 0;
            while (DownloadingThreadFactory.RunningThreads>0)
            {
                Thread.Sleep(1000);
                //Console.WriteLine($"{BytesReceived} in {TotalBytes}");
                if (DownloadingThreadFactory.RunningThreads < 8)
                {
                    annouce++;
                    if (annouce>7)
                    {
                        break;
                    }
                }
            }
            IsFinished= true;
        }

        public void Start(string tempFolder = null)
        {
            if (string.IsNullOrEmpty(tempFolder))
            {
                tempFolder = Path.Combine(Path.GetTempPath(), "CoreLaunching");
            }
            new Thread(() =>
            {
                for (int i = 0; i < Items.Count(); i++)
                {
                    if(_queue.Count==0)
                    {
                        continue;
                    }
                    var info = _queue.Dequeue();
                    if (info == null)
                    {
                        continue;
                    }
                    foreach (var item in info.GetThread(this, tempFolder))
                    {
                        while (DownloadingThreadFactory.RunningThreads > DownloadingThreadFactory.EnabledThreads)
                        {
                            Thread.Sleep(100);
                        }
                        item.Start();
                        DownloadingThreadFactory.RunningThreads++;
                        if (DownloadingThreadFactory.Break)
                        {
                            break;
                        }
                    }
                }
                IsFinished = true;
            }

            ).Start();

        }
    }
}
