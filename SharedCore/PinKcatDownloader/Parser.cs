﻿using CoreLaunching.JsonTemplates;
using Newtonsoft.Json;
using File = System.IO.File;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using CoreLaunching.MicrosoftAuth;
using System.IO;
using CoreLaunching.DownloadAPIs.Universal;

namespace CoreLaunching.PinKcatDownloader
{
    //[Obsolete("不是你用个🥚的 mcbbs 啊")]
    public class Parser
    {
        private string _versionInfo = DownloadSources.MOJANG_Version_Manifest;

        public string VersionInfo
        {
            get { return _versionInfo; }
            set {
                if (value.EndsWith("/")){
                    value = value.Substring(0, value.Length - 1);
                }
                _versionInfo = value
                    ; }
        }
        private string _assetsSource = DownloadSources.MOJANG_Assets_Source; //"https://download.mcbbs.net/assets";

        public string AssetsSource
        {
            get { return _assetsSource; }
            set {
                if (value.EndsWith("/"))
                {
                    value = value.Substring(0, value.Length - 1);
                }
                _assetsSource = value;
            }
        }

        private string _librarySource = DownloadSources.MOJANG_Libraries; // "https://download.mcbbs.net/maven/";

        public string LibrarySource
        {
            get { return _librarySource; }
            set
            {
                if (!value.EndsWith("/"))
                {
                    value += "/";
                }
                _librarySource = value;
            }
        }
        private string _forgeLibrarySource = DownloadSources.FORGE_Forge_Libraries; //"https://download.mcbbs.net/maven/";

        public string ForgeLibrarySource
        {
            get { return _forgeLibrarySource; }
            set
            {
                if (!value.EndsWith("/"))
                {
                    value += "/";
                }
                _forgeLibrarySource = value;
            }
        }

        public MCFileInfo ParseClient(string contentOrPath, ParseType type, string dotMCFolder, string customVersionName, bool RemoveExists)
        {
            Root root = null;
            if (type == ParseType.Json)
            {
                root = JsonConvert.DeserializeObject<Root>(contentOrPath);
            }
            else if (type == ParseType.FilePath)
            {
                root = JsonConvert.DeserializeObject<Root>(File.ReadAllText(contentOrPath));
            }
            else if (type == ParseType.NativeUrl)
            {
                using (var clt = new WebClient())
                {
                    root = JsonConvert.DeserializeObject<Root>(clt.DownloadString(contentOrPath));
                }
            }
            if (root == null)
            {
                throw new ArgumentException("无法解析。");
            }
            else
            {
                #region Client
                var cltinf = root.Downloads.Client;
                var clturl = string.Empty;
                if (root.InheritsFrom != null)
                {
                    clturl = $"https://download.mcbbs.net/version/{root.InheritsFrom}/client";
                }
                else
                {
                    clturl = $"https://download.mcbbs.net/version/{root.Id}/client";
                }
                #region Redirect
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(clturl);
                req.Method = "HEAD";
                req.AllowAutoRedirect = false;
                using (WebResponse response = req.GetResponse())
                {
                    clturl = "https://download.mcbbs.net" + response.Headers["Location"];
                }
                #endregion
                var cltlocal = Path.Combine(dotMCFolder, "versions", customVersionName, $"{customVersionName}.jar");
                if (RemoveExists)
                {

                    var sha1 = Certutil.Sha1(cltlocal);
                    if (!Certutil.CompareLength(cltinf.Size, cltlocal))
                    {
                        return (new("client", cltinf.Sha1, cltinf.Size, clturl, cltlocal));
                    }
                }
                else
                {
                    return (new("client", cltinf.Sha1, cltinf.Size, clturl, cltlocal));
                }
                #endregion
            }
            return null;
        }

        public IEnumerable<MCFileInfo> ParseLibraries(string contentOrPath, ParseType type, string dotMCFolder, string customVersionName, bool RemoveExists)
        {
            List<MCFileInfo> res = new();
            Root root = null;
            if (type == ParseType.Json)
            {
                root = JsonConvert.DeserializeObject<Root>(contentOrPath);
            }
            else if (type == ParseType.FilePath)
            {
                root = JsonConvert.DeserializeObject<Root>(File.ReadAllText(contentOrPath));
            }
            else if (type == ParseType.NativeUrl)
            {
                using (var clt = new WebClient())   
                {
                    root = JsonConvert.DeserializeObject<Root>(clt.DownloadString(contentOrPath));
                }
            }
            if (root == null)
            {
                throw new ArgumentException("无法解析。");
            }
            else
            {
                #region Client
                if (root.Downloads != null)
                {

                    var cltinf = root.Downloads.Client;
                    var clturl = string.Empty;
                    if (root.InheritsFrom != null)
                    {
                        clturl = $"https://download.mcbbs.net/version/{root.InheritsFrom}/client";
                    }
                    else
                    {
                        clturl = $"https://download.mcbbs.net/version/{root.Id}/client";
                    }
                    #region Redirect
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(clturl);
                    req.Method = "HEAD";
                    req.AllowAutoRedirect = false;
                    using (WebResponse response = req.GetResponse())
                    {
                        clturl = "https://download.mcbbs.net" + response.Headers["Location"];
                    }
                    #endregion
                    var cltlocal = Path.Combine(dotMCFolder, "versions", customVersionName, $"{customVersionName}.jar");
                    if (RemoveExists)
                    {

                        var sha1 = Certutil.Sha1(cltlocal);
                        if (!Certutil.CompareLength(cltinf.Size, cltlocal))
                        {
                            res.Add(new("client", cltinf.Sha1, cltinf.Size, clturl, cltlocal));
                        }
                    }
                    else
                    {
                        res.Add(new("client", cltinf.Sha1, cltinf.Size, clturl, cltlocal));
                    }
                }
                #endregion
                foreach (var item in root.Libraries)
                {
                    if (item.Downloads.Artifact != null)
                    {
                        var native = item.Downloads.Artifact.Url.Replace("https://libraries.minecraft.net/", LibrarySource);
                        native = item.Downloads.Artifact.Url.Replace("https://maven.minecraftforge.net/", ForgeLibrarySource);
                        var local = Path.Combine(dotMCFolder, "libraries", item.Downloads.Artifact.Path.Replace("/", "\\"));
                        var size = item.Downloads.Artifact.Size;
                        var name = item.Name;
                        var sha1 = item.Downloads.Artifact.Sha1;
                        if (RemoveExists)
                        {
                            if (Certutil.CompareLength(size, local))
                            {
                                var sha1_ = Certutil.Sha1(local);
                                if(sha1_ != sha1)
                                {
                                    res.Add(new(name, sha1, size, native, local));
                                }
                            }
                            else
                            {
                                res.Add(new(name, sha1, size, native, local));
                            }
                        }
                        else
                        {
                            res.Add(new(name, sha1, size, native, local));
                        }
                    }
                    if (item.Downloads.Classifiers != null)
                    {
                        for (int j = 0; j < item.Downloads.Classifiers.Count; j++)
                        {
                            var classifier = item.Downloads.Classifiers[j];
                            var name = item.Name;
                            var classnative = classifier.Item.Url;
                            var classlocal = Path.Combine(dotMCFolder, "libraries", classifier.Item.Path.Replace("/", "\\"));
                            var size = classifier.Item.Size;
                            var sha1 = classifier.Item.Sha1;
                            if (RemoveExists)
                            {
                                if (!Certutil.CompareLength(size, classlocal))
                                {
                                    res.Add(new(name, sha1, size, classnative, classlocal));
                                }
                                else
                                {
                                    var sha1_ = Certutil.Sha1(classlocal);
                                    if (sha1_ != sha1)
                                    {
                                        res.Add(new(name, sha1, size, classnative, classlocal));
                                    }
                                }
                            }
                            else
                            {
                                res.Add(new(name, sha1, size, classnative, classlocal));
                            }
                        }
                    }
                }
            }
            return res.Distinct(new InfoCompare());
        }

        [Obsolete("建议使用 ParseLibraries() 这样的方法分开解析")]
        public MCFileInfo[] ParseFromJson(string contentOrPath,ParseType type,string dotMCFolder,string customVersionName,bool removeLocal)
        {
            List<MCFileInfo> res = new();
            Root root= null;
            if(type==ParseType.Json)
            {
                root=JsonConvert.DeserializeObject<Root>(contentOrPath);
            }
            else if(type==ParseType.FilePath)
            {
                root = JsonConvert.DeserializeObject<Root>(File.ReadAllText(contentOrPath));
            }
            else if (type==ParseType.NativeUrl)
            {
                using (var clt = new WebClient())
                {
                    root = JsonConvert.DeserializeObject<Root>(clt.DownloadString(contentOrPath));
                }
            }
            if (root == null)
            {
                throw new ArgumentException("无法解析。");
            }
            else
            {
                using (var clt = new WebClient())
                {
                    var strin = clt.DownloadString(root.AssetIndex.Url);
                    //var indLocal = Path.Combine(dotMCFolder, "assets\\indexes",Path.GetFileName(root.AssetIndex.Url));
                    //Directory.CreateDirectory(Path.GetDirectoryName(indLocal));
                    //File.Create(indLocal).Close();
                    //File.WriteAllText(indLocal,strin);
                    var Assets = JsonConvert.DeserializeObject<AssetsObject>(strin);
                    foreach (var item in Assets.Objects)
                    {
                        var addOne = new MCFileInfo(item, AssetsSource, dotMCFolder);
                        if (removeLocal)
                        {
                            //var sha1 = Certutil.Sha1(addOne.Local);
                            if (!File.Exists(addOne.Local))
                            {
                                res.Add(addOne);
                            }
                        }
                        else
                        {
                            res.Add(addOne);
                        }
                    }
                }
                foreach (var item in root.Libraries)
                {
                    if (item.Downloads.Artifact != null)
                    {
                        var native = item.Downloads.Artifact.Url.Replace("https://libraries.minecraft.net/",LibrarySource);
                        native = item.Downloads.Artifact.Url.Replace("https://maven.minecraftforge.net/", ForgeLibrarySource);
                        var local = Path.Combine(dotMCFolder, "libraries", item.Downloads.Artifact.Path.Replace("/", "\\"));
                        var size = item.Downloads.Artifact.Size;
                        var name = item.Name;
                        var sha1 = item.Downloads.Artifact.Sha1;
                        if (removeLocal)
                        {
                            var sha1_ = Certutil.Sha1(local);
                            if (!Certutil.CompareLength(size,local))
                            {
                                res.Add(new(name, sha1, size, native, local));
                            }
                        }
                        else
                        {
                            res.Add(new(name, sha1, size, native, local));
                        }
                    }
                    if (item.Downloads.Classifiers != null)
                    {
                        for (int j = 0; j < item.Downloads.Classifiers.Count; j++)
                        {
                            var classifier = item.Downloads.Classifiers[j];
                            var name = item.Name;
                            var classnative = classifier.Item.Url;
                            var classlocal = Path.Combine(dotMCFolder, "libraries", classifier.Item.Path.Replace("/", "\\"));
                            var size = classifier.Item.Size;
                            var sha1 = classifier.Item.Sha1;
                            if (removeLocal)
                            {
                                var sha1_ = Certutil.Sha1(classlocal);
                                if (!Certutil.CompareLength(size,classlocal))
                                {
                                    res.Add(new(name, sha1, size, classnative, classlocal));
                                }
                            }
                            else
                            {
                                res.Add(new(name, sha1, size, classnative, classlocal));
                            }
                        }
                    }
                }
            }
            return res.Distinct(new InfoCompare()).ToArray();
        }

        public IEnumerable<MCFileInfo> ParseAssetsFromIndexUrl(string index,string contentOrPath, ParseType type, string dotMCFolder,string sha1,bool removeLocal=true)
        {

            List<MCFileInfo> res = new();
            AssetsObject assets = null;
            if (type == ParseType.Json)
            {
                assets = JsonConvert.DeserializeObject<AssetsObject>(contentOrPath);
            }
            else if (type == ParseType.FilePath)
            {
                assets = JsonConvert.DeserializeObject<AssetsObject>(File.ReadAllText(contentOrPath));
            }
            else if (type == ParseType.NativeUrl)
            {
                using (var clt = new WebClient())
                {
                    var local = Path.Combine(dotMCFolder, "assets\\indexes", $"{index}.json");
                    var str = clt.DownloadString(contentOrPath);
                    Directory.CreateDirectory(Path.GetDirectoryName(local));
                    try
                    {
                        if (!File.Exists(local))
                        {
                            File.Create(local);
                            File.WriteAllText(local, str);
                        }
                        else if (!Certutil.CompareSha1(sha1, local))
                        {
                            File.WriteAllText(local, str);
                        }
                    }
                    catch
                    {

                    }
                    assets = JsonConvert.DeserializeObject<AssetsObject>(str);
                }
            }
            if (assets == null)
            {
                throw new ArgumentException("无法解析。");
            }
            else
            {
                foreach (var item in assets.Objects)
                {
                    var addOne = new MCFileInfo(item, AssetsSource, dotMCFolder);
                    if (removeLocal)
                    {
                        if (!File.Exists(addOne.Local))
                        {
                            res.Add(addOne);
                        }
                    }
                    else
                    {
                        res.Add(addOne);
                    }
                }
            }
            return res;
        }
    }

    public enum ParseType
    {
        Json = 0,
        FilePath = 1,
        NativeUrl =2,
        Stream = 3
    }
}
