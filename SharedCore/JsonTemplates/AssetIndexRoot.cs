﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CoreLaunching.DownloadAPIs.Universal;
using CoreLaunching.PinKcatDownloader;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CoreLaunching.JsonTemplates
{
    public class AssetsObject : IDownloadable
    {
        [JsonConverter(typeof(NameHashSizeConvert))]
        [JsonProperty("objects")]
        public List<NameHashSize> Objects;

        public IEnumerable<MCFileInfo> GetFiles(string targetFolder, bool ignoreExist, Dictionary<string, string> sources = null)
        {
                sources = sources ?? DownloadSources.GetBmclapiAPI();
            var lst = new List<MCFileInfo>();
            var sourc = sources["${assets}"];
            if (sourc.EndsWith('/'))
            {
                sourc = sourc[..^1];
            }
            foreach (var item in Objects)
            {
                var lsti = new MCFileInfo(item, sourc,targetFolder);
                if (ignoreExist && File.Exists(lsti.Local))
                {
                    continue;
                }
                else
                {
                    lst.Add(lsti);
                }
            }
            return lst;
        }
    }

    public class NameHashSizeConvert : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override object? ReadJson(JsonReader reader, Type objectType, object? existingValue, JsonSerializer serializer)
        {
            List<NameHashSize> list = new List<NameHashSize>();
            foreach (var item in (JObject)(JToken.ReadFrom(reader)))
            {
                NameHashSize nhs = new NameHashSize();
                nhs.Name = item.Key;
                nhs.Hash = (string)item.Value["hash"];
                nhs.Size = (int)item.Value["size"];
                list.Add(nhs);
            }
            return list;
        }

        public override void WriteJson(JsonWriter writer, object? value, JsonSerializer serializer)
        {

        }
    }

    public class NameHashSize:Object
    {
        public string Name;
        public string Hash;
        public int Size;
        public override string ToString()
        {
            return Name;
        }
    }
}
