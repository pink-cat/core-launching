﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Reflection;
using HtmlAgilityPack;
using System.Runtime.InteropServices;
using CoreLaunching.PinKcatDownloader;
using System.Net;
using CoreLaunching.DownloadAPIs.Universal;
using static System.Net.WebRequestMethods;

namespace CoreLaunching.JsonTemplates
{
    public class Rule
    {
        [JsonIgnore]
        public bool IsAllowed => Action == "allow" && RequiredValue == Value;
        [JsonIgnore]
        public string FeatureName { get; set; }
        public bool Value { get; set; }
        [JsonProperty("action")]
        public string Action { get; set; }
        public bool RequiredValue { get; set; }
        [JsonIgnore]
        public JToken JObject { get; internal set; }

        public override string ToString()
        {
            return FeatureName + " : " + RequiredValue;
        }
    }

    internal class RulesConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override object? ReadJson(JsonReader reader, Type objectType, object? existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object? value, JsonSerializer serializer)
        {
            if(value is IEnumerable<Rule> rule)
            {
                writer.WriteStartArray();
                foreach (var ru in rule)
                {
                    writer.WriteRawValue(ru.JObject.ToString());
                }
                writer.WriteEndArray();
            }
        }
    }

    public interface IValuePair
    {
        [JsonIgnore]
        public JToken JObject { get; internal set; }
        public bool Allow { get; }
        public List<string> Values { get; set; }
    }

    public class NotSystemValuePair : IValuePair
    {
        [JsonConverter(typeof(RulesConverter))]
        [JsonProperty("rules")]
        public List<Rule> Rules { get; set; }
        [JsonProperty("values")]
        public List<string> Values { get; set; }
        [JsonIgnore]
        public bool Allow => Vaild();
        public JToken JObject { get; set; }

        private bool Vaild()
        {
            var res = true;
            foreach (var item in Rules)
            {
                if (item.IsAllowed == false)
                {
                    res = false; break;
                }
            }
            return res;
        }

        public NotSystemValuePair()
        {
            Rules = new List<Rule>();
            Values = new List<string>();
        }

        public override string ToString()
        {
            var res = "";
            foreach (var rule in Rules)
            {
                res += rule.ToString();
                res += Environment.NewLine;
            }
            res += "*****";
            res += Environment.NewLine;
            foreach (var value in Values)
            {
                res += $"{value} {Environment.NewLine}";
            }
            return res;
        }
    }
    public class SystemValuePair:IValuePair
    {
        public JToken JObject { get ; set ; }
        [JsonIgnore]
        public bool Allow { get; set; }
        [JsonProperty("values")]
        public List<string> Values { get; set; }
        public SystemValuePair()
        {
            Values = new();
        }

        public override string ToString()
        {
            var res = "";
            if (Allow)
            {
                foreach (var item in Values)
                {
                    res += $"{item} ";
                }
            }
            return res;
        }
    }

    public interface IProvideELPair
    {
        [JsonIgnore]
        public Dictionary<string, string> ELPair { get; set; }
    }
    public interface IArgumentsProvider
    {

        public string ToArgument(Dictionary<string, string> eLPair);
        public bool TryToArgument(Dictionary<string, string> diction, out string arg);
    }
    public interface IArguments : IProvideELPair,IArgumentsProvider
    {
    }
    public class VersionJson : IVersionJson
    {
        public string InheritsFrom { get; set; }
        public TwoPartArguments Arguments { get; set; }
        public AssetIndex AssetIndex { get; set; }
        public string Assets { get; set; }
        public int ComplianceLevel { get; set; }
        public Downloads Downloads { get; set; }
        public string Id { get; set; }
        public JavaVersion JavaVersion { get; set; }
        public VersionInfoLibraries Libraries { get; set; }
        public Logging Logging { get; set; }
        public string MainClass { get; set; }
        public int MinimumLauncherVersion { get; set; }
        public string ReleaseTime { get; set; }
        public string Time { get; set; }
        public string Type { get; set; }
        public Dictionary<string, JToken> ReamaindProperties { get ; set ; }
        public Dictionary<string, string> ELPair { get; set; }

        public void PreLaunchAction(Dictionary<string, string> eLPairs)
        {
            throw new NotImplementedException();
        }

        public string ToArgument(Dictionary<string, string> diction)
        {
            return Arguments.ToArgument(diction);
        }

        public bool TryToArgument(Dictionary<string, string> diction,out string arg)
        {
            arg=ToArgument(diction);
            return false;
        }
        /// <summary>
        /// 这个方法已不再返回包括资源文件的清单，要下载资源，请查看 <see cref="AssetIndex"/>
        /// </summary>
        /// <param name="targetFolder">直接填入 .minecraft 文件夹路径</param>
        /// <param name="sources">下载源，不填就是官方源。</param>
        /// <returns>文件清单</returns>
        public IEnumerable<MCFileInfo> GetFiles(string targetFolder, bool ignoreExist = true, Dictionary<string, string> sources = null)
        {
                sources = sources ?? DownloadSources.GetMojangAPI();
            var lst = new List<MCFileInfo>();
            //var assetsPath = Path.Combine(targetFolder, "assets");
            var jarp = Path.Combine(targetFolder, "versions", this.Id);
            if (sources.ContainsKey("${versions_api}"))
            {
                Downloads.Client.Url = sources["${versions_api}"];
                var url = Downloads.Client.Url.Replace("${clientVersion}", Downloads.Client.Id);
                url = url.Replace("${version}", Downloads.Client.Id);
                if (string.IsNullOrEmpty(Downloads.Client.Id))
                {
                    Downloads.Client.Url = $"https://bmclapi2.bangbang93.com/v1/objects/{Downloads.Client.Sha1}/client.jar";
                }
                else
                {
                    Downloads.Client.Url = LinkUtil.Redirect(url);
                }
            }
            lst.Add(new(this.Id, Downloads.Client.Sha1, Downloads.Client.Size, Downloads.Client.Url, Path.Combine(jarp, $"{this.Id}.jar")));
            using (var clt = new WebClient())
            {
                var assetsj = clt.DownloadString(this.AssetIndex.Url);
                //var dire = Directory.CreateDirectory(Path.Combine(assetsPath, "indexes"));
                //using (var fs = File.CreateText(Path.Combine(dire.FullName, $"{AssetIndex.Id}.json")))
                //{
                //    fs.AutoFlush = true;
                //    fs.Write(assetsj);
                //    assetsPath = Path.Combine(assetsPath, "objects");
                //    var assetIns = JsonConvert.DeserializeObject<AssetsObject>(assetsj);
                //    lst.AddRange(assetIns.GetFiles(targetFolder, sources));
                //}
            }
            lst.AddRange(Libraries.GetFiles(targetFolder, ignoreExist,sources));
            for (int i = 0; i < lst.Count; i++)
            {
                if (System.IO.File.Exists(lst[i].Local))
                {
                    lst.RemoveAt(i);
                    i--;
                }
            }
            return lst;
        }

        public IEnumerable<MCFileInfo> GetLibriesUrls(string targetFolder,bool ignoreExist=true, Dictionary<string, string> sources = null)
        {
            return Libraries.GetFiles(targetFolder, ignoreExist,sources).ToList();
        }
    }
    public class VersionJsonConvertor : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override object? ReadJson(JsonReader reader, Type objectType, object? existingValue, JsonSerializer serializer)
        {
            var obj = JObject.ReadFrom(reader);
            var versionJson = new VersionJson(); 

            var diction = new Dictionary<string, JToken>();
            foreach (JProperty item in obj)
            {
                diction[item.Name.ToLower()] = item.Value;
            }

            PropertyInfo[] properties = objectType.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty);
            foreach (var prop in properties)
            {
                var at = prop.GetCustomAttributes(false);
                foreach (var iface in objectType.GetInterfaces())
                {

                    if (iface.GetProperty(prop.Name) is PropertyInfo iprop)
                    {
                        at = iprop.GetCustomAttributes(false);
                        break;
                    }
                }
                if (at.Where(x => x is JsonIgnoreAttribute).Count() > 0)
                {
                    continue;
                }
                var jpropname = prop.Name.ToLower();
                var jatr = at.Where(x => x is JsonPropertyAttribute).FirstOrDefault();
                if (jatr is JsonPropertyAttribute jpropatr)
                {
                    jpropname = jpropatr.PropertyName.ToLower();
                }
                if (diction.ContainsKey(jpropname))
                {
                    var value = diction[jpropname];

                    Type propType = prop.PropertyType;
                    object propValue = null;
                    if (propType == typeof(string))
                    {
                        propValue = value.ToString();
                    }
                    else
                    {
                        var attr = at.Where(x => (x as Attribute).GetType() == typeof(JsonConverterAttribute));
                        if (attr.Count() > 0)
                        {
                            List<JsonConverter> c = new();
                            foreach (JsonConverterAttribute item in attr)
                            {
                                c.Add(Activator.CreateInstance(item.ConverterType) as JsonConverter);
                            }

                            propValue = JsonConvert.DeserializeObject(value.ToString(), propType, c.ToArray());
                        }
                        else
                        {
                            propValue = JsonConvert.DeserializeObject(value.ToString(), propType);

                        }
                    }
                    prop.SetValue(versionJson, propValue);
                    diction.Remove(jpropname);
                }
            }
            versionJson.ReamaindProperties = diction;
            foreach (var kvp in versionJson.ReamaindProperties)
            {
                if (kvp.Key == "minecraftarguments")
                {
                    var list = kvp.Value.ToString().ToStringList();
                    versionJson.Arguments = new() { Game = new(),Jvm=new()};
                    foreach (var item in list)
                    {
                        versionJson.Arguments.Game.Add(item.ToString());
                        if (item.StartsWith('$'))
                        {
                            versionJson.Arguments.ELPair.Add(item, item);
                        }
                    }
                    versionJson.ReamaindProperties.Remove(kvp.Key);
                }
            }
            if(obj["downloads"]["client"]["id"] is JToken jk)
            {
                versionJson.Downloads.Client.Id = jk.ToString();
            }
            return versionJson;
        }

        public override void WriteJson(JsonWriter writer, object? value, JsonSerializer serializer)
        {
            if(value is IVersionJson vj)
            {
                writer.WriteStartObject();
                var objectType = vj.GetType();
                PropertyInfo[] properties = objectType.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty);
                foreach (var prop in properties)
                {
                    var at = prop.GetCustomAttributes(false);
                    foreach (var iface in objectType.GetInterfaces())
                    {

                        if (iface.GetProperty(prop.Name) is PropertyInfo iprop)
                        {
                            at = iprop.GetCustomAttributes(false);
                            break;
                        }
                    }
                    if (at.Where(x => x is JsonIgnoreAttribute).Count() > 0)
                    {
                        continue;
                    }
                    var jpropname = prop.Name.Trim()[..1].ToLower() + prop.Name.Trim()[1..];
                    var jvalue = prop.GetValue(value);
                    if(jvalue != null)
                    {

                        writer.WritePropertyName(jpropname);
                        writer.WriteRawValue(JsonConvert.SerializeObject(jvalue));
                    }
                }
                writer.WriteEndObject();
            }
        }
    }

    public class LibrariesArgumentsConverter : IArgumentConverter
    {
        public bool CanConvert(Type targetType)
        {
            return true;
        }

        public string Convert(object source, Type targetType,Dictionary<string,string> eLPairs)
        {
            return "";
        }

        public object ConvertBack(string source, Type targetType)
        {
            throw new NotImplementedException();
        }
    }
    public static class VersionJsonUtil
    {

        public static bool TryParse(ParseType type, string path, out VersionJson result, string info)
        {
            result = null;
            info = "";
            try
            {
                result = Parse(type, path);
                return true;
            }
            catch (Exception ex)
            {
                info = ex.Message;
                return false;
            }
        }
        public static bool TryParse<T>(ParseType type, string path, out T result, string info) where T : IVersionJson
        {
            result = default(T);
            info = "";
            try
            {
                result = Parse<T>(type, path);
                return true;
            }
            catch (Exception ex)
            {
                info = ex.Message;
                return false;
            }
        }
        public static VersionJson Parse(ParseType type, string path)
        {
            return Parse<VersionJson>(type,path);
        }
        public static T Parse<T>(ParseType type, string path) where T:IVersionJson
        {
            var text = path;
            if (type == ParseType.FilePath)
            {
                text = System.IO.File.ReadAllText(path);
            }
            else if (type == ParseType.NativeUrl)
            {
                using (var webc = new WebClient())
                {
                    text = webc.DownloadString(path);
                }
            }
            return JsonConvert.DeserializeObject<T>(text);
        }

        public static string ReplaceName(string str, string customGameName)
        {
            var jOb = JObject.Parse(str);
            jOb["downloads"]["client"]["id"] = jOb["id"];
            jOb["id"] = customGameName;
            return jOb.ToString();
        }
    }

    [JsonConverter(typeof(VersionJsonConvertor))]
    public interface IVersionJson:IArguments,IDownloadable,IRequireLibs
    {
        public void PreLaunchAction(Dictionary<string, string> eLPairs);
        [JsonProperty("inheritsFrom")]
        public string InheritsFrom { get; set; }
        [JsonProperty("arguments")]
        public TwoPartArguments Arguments { get; set; }

        public AssetIndex AssetIndex { get; set; }

        public string Assets { get; set; }

        public int ComplianceLevel { get; set; }

        public Downloads Downloads { get; set; }
        public string Id { get; set; }
        public JavaVersion JavaVersion { get; set; }
        [JsonProperty("libraries")]
        public VersionInfoLibraries Libraries { get; set; }
        public Logging Logging { get; set; }
        [ELExpresion("${mainclassName}")]
        public string MainClass { get; set; }
        public int MinimumLauncherVersion { get; set; }
        public string ReleaseTime { get; set; }
        public string Time { get; set; }
        public string Type { get; set; }
        [JsonIgnore]
        public Dictionary<string, JToken> ReamaindProperties { get; set; }
    }

    public interface IRequireLibs
    {
        IEnumerable<MCFileInfo> GetLibriesUrls(string targetFolder,bool ignoreExist = true, Dictionary<string, string> sources = null);
    }

    public class LibraryInfoConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override object? ReadJson(JsonReader reader, Type objectType, object? existingValue, JsonSerializer serializer)
        {
            var jOb = JObject.Load(reader);
            var lib = new LibraryInfo();
            lib.Allowed = true;
            foreach (var item in jOb)
            {
                if (jOb["name"] is JToken name)
                {
                    lib.Name = name.ToString();
                }
                if (jOb["downloads"] is JToken downloads)
                {
                    lib.Downloads = new();
                    if (downloads.Count()<1)
                    {

                    }
                    else if (downloads?["classifiers"] is JObject classfs)
                    {
                        lib.Downloads.Classifiers = new();
                        foreach (JProperty prop in classfs.Properties())
                        {
                            var key = prop.Name.ToString();
                            var value = JsonConvert.DeserializeObject<Artifact>(prop.Value.ToString());
                            lib.Downloads.Classifiers.Add(new() { Item=value,Platform=key});
                        }
                    }
                    else if (downloads["artifact"] is JToken arti)
                    {
                        lib.Downloads.Artifact = JsonConvert.DeserializeObject<Artifact>(downloads["artifact"].ToString());
                    }
                }
                if (jOb["extract"] is JToken extract)
                {
                    lib.Extract = JsonConvert.DeserializeObject<Extract>(extract.ToString());
                }
                if (jOb["rules"] is JArray rules)
                {
                    lib.RuleJson = rules;
                    foreach (var rule in rules)
                    {
                        if (rule["os"] is JToken os)
                        {
                            var satis = false;
                            if (os["name"] is JToken osname)
                            {
                                if (osname.ToString().Contains("windows") && RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                                {
                                    satis = true;
                                }
                                else if (osname.ToString().Contains ( "osx" )&& RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                                {
                                    satis = true;
                                }
                                else if (osname.ToString().Contains ( "linux") && RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                                {
                                    satis = true;
                                }
                            }
                            if (os["arch"] is JToken arch)
                            {
                                var require64 = arch.ToString().Contains("64") || System.Runtime.InteropServices.RuntimeInformation.ProcessArchitecture==Architecture.Arm;
                                var arm = arch.ToString().Contains("arm");
                                satis = (!(require64 ^ System.Environment.Is64BitOperatingSystem))&& 
                                    (!(arm ^ (RuntimeInformation.ProcessArchitecture==Architecture.Arm||
                                    System.Runtime.InteropServices.RuntimeInformation.ProcessArchitecture==Architecture.Arm64)));
                            }
                            lib.Allowed = !(satis ^ rule["action"].ToString() == "allow");
                        }
                        else
                        {
                            lib.Allowed = rule["action"].ToString() == ("allow");
                        }
                    }
                }
            }
            return lib;
        }

        public override void WriteJson(JsonWriter writer, object? value, JsonSerializer serializer)
        {

            if (value is LibraryInfo li)
            {
                writer.WriteStartObject();
                var objectType = li.GetType();
                PropertyInfo[] properties = objectType.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty);
                foreach (var prop in properties)
                {
                    var at = prop.GetCustomAttributes(false);
                    foreach (var iface in objectType.GetInterfaces())
                    {

                        if (iface.GetProperty(prop.Name) is PropertyInfo iprop)
                        {
                            at = iprop.GetCustomAttributes(false);
                            break;
                        }
                    }
                    if (at.Where(x => x is JsonIgnoreAttribute).Count() > 0)
                    {
                        continue;
                    }
                    var jpropname = prop.Name.Trim()[..1].ToLower() + prop.Name.Trim()[1..];
                    var jvalue = prop.GetValue(value);
                    if (jpropname.Contains("allow"))
                    {

                    }
                    else if (jpropname.Contains("rule")&&jvalue!=null)
                    {
                        writer.WritePropertyName("rules");
                        writer.WriteRawValue(JsonConvert.SerializeObject(jvalue));
                    }
                    else if (jpropname.ToLower().Contains("downloads"))
                    {
                        if (li.Downloads?.Classifiers?.Count() == 0){
                            li.Downloads.Classifiers = null;
                        }
                        else if (li.Downloads is null|| li.Downloads?.Classifiers is null)
                        {

                        }
                        else
                        {
                            var rawValue = "{";
                            for (int i = 0; i < li.Downloads?.Classifiers?.Count; i++)
                            {
                                var item = li.Downloads.Classifiers[i];
                                rawValue+= $"\"{item.Platform.Replace("natives-","")}\": \"{item.Platform}\"";
                                if ((i+1) == li.Downloads.Classifiers.Count)
                                {
                                    rawValue+= "}";
                                }
                                else
                                {
                                    rawValue += ",";
                                }
                            }
                            writer.WritePropertyName("natives");
                            writer.WriteRawValue(rawValue);
                        }
                        var jSetting = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore};
                        writer.WritePropertyName("downloads");
                        writer.WriteRawValue(JsonConvert.SerializeObject(jvalue,jSetting));
                    }
                    else if (jvalue != null)
                    {
                        writer.WritePropertyName(jpropname);
                        writer.WriteRawValue(JsonConvert.SerializeObject(jvalue));
                    }
                }
                writer.WriteEndObject();
            }
        }
    }
    [JsonConverter(typeof(LibraryInfoConverter))]
    public class LibraryInfo
    {
        public override string ToString()
        {
            if (Name != null)
            {
                return Name;
            }
            else
            {
                return base.ToString();
            }
        }

        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("extract")]
        public Extract Extract { get; set; }
        [JsonProperty("downloads")]
        public Downloads Downloads { get; set; }
        [JsonIgnore]
        public bool Allowed { get; set; }
        public JArray RuleJson { get; set; }

        public IEnumerable<MCFileInfo> GetDownloads(string libPath)
        { 
            var res = new List<MCFileInfo>();
            if (Allowed)
            {
                if (Downloads != null&&Downloads.Artifact!=null)
                {
                    var path = Path.Combine(libPath, Downloads.Artifact.Path.Replace('/', FileVerifyUtil.GetSeparator()));
                    res.Add(new(Name, Downloads.Artifact.Sha1, Downloads.Artifact.Size, Downloads.Artifact.Url, path));
                }
                if (Downloads.Classifiers != null)
                {
                    //TODO Classifier 处理
                }
            }
            return res;
        }
    }

    [ArgumentConverter(typeof(LibrariesArgumentsConverter))]
    [ELExpresion("${classpath}")]
    public class VersionInfoLibraries : List<LibraryInfo>, IDownloadable
    {
        public IEnumerable<MCFileInfo> GetFiles(string targetFolder, bool ignoreExist = true, Dictionary<string, string> sources = null)
        {
            var res = new List<MCFileInfo>();
            var libr = sources["${libraries}"];
            var libp = Path.Combine(targetFolder,"libraries");
            if(libr.EndsWith('/')){
                libr = libr[..^1];
            }
            foreach (var item in this)
            {
                var it = item.GetDownloads(libp);
                foreach (var init in it)
                {
                    init.Url = init.Url.Replace("https://libraries.minecraft.net", libr);
                }
                foreach (var ii in it)
                {
                    if (ignoreExist && System.IO.File.Exists(ii.Local))
                    {
                        continue;
                    }
                    res.Add(ii);
                }
            }
            return res;
        }
    }
    public interface IArgumentConverter
    {
        public bool CanConvert(Type targetType);
        public string Convert(object source,Type targetType,Dictionary<string, string> eLPairs);
        public object ConvertBack(string source, Type targetType);
    }
    [System.AttributeUsage(AttributeTargets.Property|AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    sealed class ELExpresionAttribute : Attribute
    {
        readonly string _key;

        // This is a positional argument
        public ELExpresionAttribute(string key)
        {
            this._key = key;
        }
        public string Key => _key;
    }
    [System.AttributeUsage(AttributeTargets.Property|AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    sealed class ArgumentConverterAttribute : Attribute
    {
        // See the attribute guidelines at 
        //  http://go.microsoft.com/fwlink/?LinkId=85236
        readonly Type _converter;

        // This is a positional argument
        public ArgumentConverterAttribute(Type converter)
        {
            _converter = converter;
        }

        public Type Converter=> _converter;

        // This is a named argument
        public int NamedInt { get; set; }
    }
    public interface IRequirRule
    {
        List<IValuePair> ValuePairs { get; set; }
    }
    [JsonConverter(typeof(PartArgumentsConverter))]
    public class PartArguments : List<string>, IArguments,IRequirRule
    {
        public void CombineArguments(PartArguments args)
        {
            foreach (var item in args)
            {
                this.Add(item);
            }
            foreach (var item in args.ValuePairs)
            {
                ValuePairs.Add(item);
            }
        }
        public PartArguments()
        {
            ELPair = new();
            ValuePairs = new();
        }
        public Dictionary<string, string> ELPair { get; set; }
        public List<IValuePair> ValuePairs { get; set; }

        public string ToArgument(Dictionary<string, string> eLPair)
        {
            var res = "";
            foreach (var item in this)
            {
                if (item.Contains(" ")) 
                {
                    res += $"\"{item}\" ";
                }
                else
                {
                    res += $"{item} ";
                }
            }
            foreach (var item in ValuePairs)
            {
                if (item.Allow)
                {
                    foreach (var value in item.Values)
                    {
                        if (value.Contains(" "))
                        {
                            res += $"\"{value}\" ";
                        }
                        else
                        {
                            res += $"{value} ";
                        }
                    }
                }
            }
            return res;
        }

        public bool TryToArgument(Dictionary<string, string> diction, out string arg)
        {
            throw new NotImplementedException();
        }
    }

    public class PartArgumentsConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override object? ReadJson(JsonReader reader, Type objectType, object? existingValue, JsonSerializer serializer)
        {
            var job = JObject.ReadFrom(reader);
            var res = new PartArguments();
            foreach (var item in job)
            {
                if(item.Type == JTokenType.String)
                {
                    var str = item.ToString();
                    res.Add(str);
                    if (str.StartsWith("$"))
                    {
                        res.ELPair.Add(str,str);
                    }
                }
                else
                {
                    IValuePair pair = null;
                    if (item["rules"] is JArray arr)
                    {
                        foreach (var ar in arr)
                        {
                            if (ar["features"] is JToken features)
                            {
                                var not = new NotSystemValuePair();
                                pair = not;
                                if (ar["features"] is JToken arr2)
                                {
                                    foreach (JProperty ft in ar["features"])
                                    {
                                        var ru = new Rule();
                                        ru.JObject = ar;
                                        ru.Action = ar["action"].ToString();
                                        ru.FeatureName = ft.Name.ToString();
                                        ru.RequiredValue = System.Convert.ToBoolean(ft.Value.ToString());
                                        not.Rules.Add(ru);
                                        not.JObject = ar;
                                    }
                                }
                            }
                            else if (ar["os"] is JToken os)
                            {
                                var sys = new SystemValuePair();
                                pair = sys;

                                var satis = false;
                                if (os["name"] is JToken name)
                                {
                                    if (name.ToString() == "windows"&& RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                                    {
                                        satis = true;
                                    }
                                    else if (name.ToString() == "osx" && RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                                    {
                                        satis = true;
                                    }
                                    else if (name.ToString() == "linux" && RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                                    {
                                        satis = true;
                                    }
                                }
                                if (os["arch"] is JToken arch)
                                {
                                    var require64 = arch.ToString().Contains("64");
                                    satis = !(require64 ^ System.Environment.Is64BitOperatingSystem);
                                }

                                sys.Allow = !(satis ^ ar["action"].ToString()=="allow");
                            }
                        }
                    }
                    if (item["value"] is JArray values)
                    {
                        foreach (var value in values)
                        {
                            var str = value.ToString();
                            pair.Values.Add(str);
                            if (str.StartsWith("$"))
                            {
                                res.ELPair.Add(str, str);
                            }
                        }
                    }
                    else if (item["value"] is JToken jp &&jp.Type is JTokenType.String)
                    {
                        pair.Values.Add(item["value"].ToString());
                    }
                    res.ValuePairs.Add(pair);
                }
            }
            return res;
        }

        public override void WriteJson(JsonWriter writer, object? value, JsonSerializer serializer)
        {
            if(value is PartArguments args)
            {
                writer.WriteStartArray();
                foreach (var str in args)
                {
                    writer.WriteValue(str);
                }
                foreach (var pair in args.ValuePairs)
                {
                    writer.WriteRawValue(JsonConvert.SerializeObject(pair));
                }
                writer.WriteEndArray();
            }
        }
    }
    public class TwoPartArguments : IArguments
    {
        public TwoPartArguments()
        {
            ELPair = new() { { "${mainclassName}", "${mainclassName}" } };
        }
        [JsonProperty("game")]
        public PartArguments Game { get; set; }
        [JsonProperty("jvm")]
        public PartArguments Jvm { get; set; }
        public Dictionary<string, string> ELPair { get ; set ; }

        public string ToArgument(Dictionary<string, string> eLPair)
        {
            var res = $"{Jvm.ToArgument(eLPair)} ${{mainclassName}} {Game.ToArgument(eLPair)}";
            return res;
        }

        public bool TryToArgument(Dictionary<string, string> diction, out string arg)
        {
            arg = "";
            try
            {
                arg = ToArgument(diction);
                return true;
            }
            catch
            {
                return false;
                throw;
            }
        }
    }

}
