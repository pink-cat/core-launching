﻿using CoreLaunching.DownloadAPIs.Universal;
using CoreLaunching.PinKcatDownloader;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace CoreLaunching.JsonTemplates
{
    public static class WebVersionInfoUtil
    {
        /// <summary>
        /// 这个方法并不会负责 Assets 的下载，请使用<see cref=""/>
        /// </summary>
        /// <param name="info"></param>
        /// <param name="dotMCPath"></param>
        /// <param name="downloadAssets"></param>
        /// <param name="ignoreExist"></param>
        /// <param name="sources"></param>
        /// <returns></returns>
        public static Task<ProcessManager> Install(this WebVersionInfo info, string dotMCPath, bool downloadAssets = false, bool ignoreExist = true, Dictionary<string, string> sources = null)
        {

            return Task.Factory.StartNew<ProcessManager>(() =>
            {
                var res = GetProcessManager(info, dotMCPath, downloadAssets, ignoreExist, sources);
                res.Start(Path.GetTempPath());
                return res;
            });
        }

        /// <summary>
        /// 下载文件列表
        /// </summary>
        /// <param name="targetFolder">直接填入 .minecraft 文件夹路径</param>
        /// <param name="sources">下载源，不填就是官方源。</param>
        /// <param name="downloadAssets">是否下载资源</param>
        /// <returns>文件清单</returns>
        public static IEnumerable<MCFileInfo> GetDownloadItems(this WebVersionInfo info, string dotMCPath, bool downloadAssets = false, bool ignoreExists = true, Dictionary<string, string> sources = null)
        {
            var id = info.Id;
            var jarp = Path.Combine(dotMCPath, "versions", id);
            for (int i = 2; Directory.Exists(jarp); i++)
            {
                id = info.Id + $" ({i})";
                jarp = Path.Combine(dotMCPath, "versions", id);
            }
            using (var webc = new WebClient())
            {
                var vtxt = webc.DownloadString(info.Url);
                var job = JObject.Parse(vtxt);
                job["id"] = id;
                Directory.CreateDirectory(jarp);
                var ttxt = job.ToString();
                File.WriteAllText(Path.Combine(jarp, $"{id}.json"), ttxt);
                var vson = VersionJsonUtil.Parse(ParseType.Json, ttxt);
                if (downloadAssets)
                {
                    return vson.GetFiles(dotMCPath, ignoreExists, sources).Concat(vson.AssetIndex.GetFiles(dotMCPath, ignoreExists, sources));
                }
                return vson.GetFiles(dotMCPath, ignoreExists, sources);
            }
        }
        public static ProcessManager GetProcessManager(this WebVersionInfo info, string dotMCPath, bool downloadAssets = false, bool ignoreExist = true, Dictionary<string, string> sources = null) => new(GetDownloadItems(info, dotMCPath, downloadAssets, ignoreExist, sources));
    }

    public class WebVersionInfo
    {
        /// <summary>
        /// (原版)游戏名称/版本号
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }
        /// <summary>
        /// (原版)游戏类型，如快照/正式
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// (原版)游戏 Json 的地址
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }
        /// <summary>
        /// (原版)游戏收录时间
        /// </summary>
        [JsonProperty("time")]
        public DateTime Time { get; set; }
        /// <summary>
        /// (原版)游戏发布时间
        /// </summary>
        [JsonProperty("releaseTime")]
        public DateTime ReleaseTime { get; set; }

#if DEBUG
        public override string ToString()
        {
            return $"{Type} : {Id}";
        }
#endif
    }

    public class OptifineFileInfo
    {

            public string Name { get; set; }
            public string MD5 { get; set; }
            public DateTime Time { get; set; }
            public string Version { get; set; }
        public bool TryGetVersion(out Version ver)
        {
            var a = System.Version.TryParse(Version,out var res);
            ver = res;
            return a;
        }

        public string GetInstallJarUrl(string api = DownloadSources.OPTIFINE_CN_API_BASE)=> string.Join("/", api, $"download/{Name}");
    }
}
