﻿using System.Collections.Generic;

namespace CoreLaunching.DownloadAPIs.Universal
{
    public static class DownloadSources
    {
        public const string BMCLAPI_Version_Manifest = "https://bmclapi2.bangbang93.com/mc/game/version_manifest.json";
        public const string BMCLAPI_AssetsIndex = "https://bmclapi2.bangbang93.com";
        public const string BMCLAPI_Assets_Source = "https://bmclapi2.bangbang93.com/assets/";
        public const string BMCLAPI_Libraries = "https://bmclapi2.bangbang93.com/maven/";
        public const string BMCLAPI_Forge_Libraries = "https://bmclapi2.bangbang93.com/maven/";
        public const string BMCLAPI_Forge_VersionManifest = "https://bmclapi2.bangbang93.com/forge/minecraft";

        public const string MCBBS_Version_Manifest = "https://download.mcbbs.net/mc/game/version_manifest.json";
        public const string MCBBS_AssetsIndex = "https://download.mcbbs.net/";
        public const string MCBBS_Assets_Source = "https://download.mcbbs.net/assets";
        public const string MCBBS_Libraries = "https://download.mcbbs.net/maven/";
        public const string MCBBS_Forge_Libraries = "https://download.mcbbs.net/maven/";

        public const string MOJANG_Version_Manifest = "http://launchermeta.mojang.com/mc/game/version_manifest.json";
        public const string MOJANG_AssetsIndex = "https://launcher.mojang.com/";
        public const string MOJANG_Assets_Source = "https://resources.download.minecraft.net";
        public const string MOJANG_Libraries = "https://libraries.minecraft.net/";
        public const string FORGE_Forge_Libraries = "https://maven.minecraftforge.net/";
        public const string FORGE_Forge_VersionManifest = "https://api.curseforge.com/v1/minecraft/modloader";

        public const string OPTIFINE_CN_API = "https://optifine.cn/api";
        public const string OPTIFINE_CN_API_BASE = "https://optifine.cn";

        public static Dictionary<string, string> GetMojangAPI()
        {
            var res = new Dictionary<string, string>()
            {
                { "${version_manifest}",DownloadSources.MOJANG_Version_Manifest },
                { "${AssIndex}",DownloadSources.MOJANG_AssetsIndex },
                { "${assets}",DownloadSources.MOJANG_Assets_Source },
                { "${forge_libraries}", DownloadSources.FORGE_Forge_Libraries },
                { "${libraries}",  DownloadSources.MOJANG_Libraries}
        };
            return res;
        }
        [Obsolete("不是你用个🥚的MCBBS啊",true)]
        public static Dictionary<string, string> GetMCBBSAPI(string clientVersion = null)
        {
            var res = new Dictionary<string, string>()
            {{ "${base_url}", "https://download.mcbbs.net" },
                { "${version_manifest}",DownloadSources.MCBBS_Version_Manifest },
                { "${AssIndex}",DownloadSources.MCBBS_AssetsIndex },
                { "${assets}",DownloadSources.MCBBS_Assets_Source },
                { "${forge_libraries}", DownloadSources.MCBBS_Forge_Libraries },
                { "${libraries}",  DownloadSources.MCBBS_Libraries},
                {"${versions_api}", "https://download.mcbbs.net/version/${clientVersion}/client" }
            };
            return res;
        }
        public static Dictionary<string, string> GetBmclapiAPI()
        {
            var res = new Dictionary<string, string>()
            {{ "${base_url}", "https://bmclapi2.bangbang93.com" },
                { "${version_manifest}",DownloadSources.BMCLAPI_Version_Manifest },
                { "${AssIndex}",DownloadSources.BMCLAPI_AssetsIndex },
                { "${assets}",DownloadSources.BMCLAPI_Assets_Source },
                { "${forge_libraries}", DownloadSources.BMCLAPI_Forge_Libraries },
                { "${libraries}",  DownloadSources.BMCLAPI_Libraries},
                { "${versions_api}", "https://bmclapi2.bangbang93.com/version/${clientVersion}/client"}
            };
            return res;
        }
    }
}
