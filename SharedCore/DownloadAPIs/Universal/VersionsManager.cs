﻿using CoreLaunching.DownloadAPIs.Forge;
using CoreLaunching.JsonTemplates;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
/* 项目“CoreLaunchingNetFw452”的未合并的更改
在此之前:
using System.Text;
using CoreLaunching.DownloadAPIs.Forge;
在此之后:
using System.Text;
*/

/* 项目“CoreLaunchingNet6 (net7.0)”的未合并的更改
在此之前:
using System.Text;
using CoreLaunching.DownloadAPIs.Forge;
在此之后:
using System.Text;
*/

/* 项目“CoreLaunchingNet6 (net6.0)”的未合并的更改
在此之前:
using System.Text;
using CoreLaunching.DownloadAPIs.Forge;
在此之后:
using System.Text;
*/

/* 项目“CoreLaunchingNet6 (net8.0)”的未合并的更改
在此之前:
using System.Text;
using CoreLaunching.DownloadAPIs.Forge;
在此之后:
using System.Text;
*/


namespace CoreLaunching.DownloadAPIs.Universal
{
    public static class VersionsManager
    {
        public static List<WebVersionInfo> WebVersionInfosCache;
        public static List<OptifineFileInfo> OptiFineFilesCache;
        public static List<IForgeVersionInfo> ForgeVersionsInfo;
        public static bool TryGetForgeList(out List<IForgeVersionInfo> list, string source = DownloadSources.BMCLAPI_Forge_VersionManifest, bool refresh = false)
        {
            list = null;
            try
            {
                list = GetForgeList(source, refresh);
                return true;
            }
            catch (WebException ex)
            {

            }
            return false;
        }

        public static List<IForgeVersionInfo> GetForgeList(string source = DownloadSources.BMCLAPI_Forge_VersionManifest, bool refresh = false)
        {
            if (refresh || ForgeVersionsInfo == null)
            {
                if (source == DownloadSources.BMCLAPI_Forge_VersionManifest)
                {
                    ForgeVersionsInfo = new();
                    foreach (var item in BMCLForgeDownloadAPI.GetSupporttedVersions())
                    {
                        var we = BMCLForgeDownloadAPI.GetKnownUrlsFromMcVersion(item);
                        ForgeVersionsInfo.AddRange(we);
                    }
                }
                else if (source == DownloadSources.FORGE_Forge_VersionManifest)
                {
                    using (var clt = new WebClient())
                    {
                        var jt = JToken.Parse(clt.DownloadString(source));
                        var _f = JsonConvert.DeserializeObject<List<ForgeVersionInfoInstance>>(jt["data"].ToString());
                        if (ForgeVersionsInfo is null)
                        {
                            ForgeVersionsInfo = new();
                        }
                        else
                        {
                            ForgeVersionsInfo.Clear();
                        }
                        foreach (var item in _f)
                        {
                            ForgeVersionsInfo.Add(item);
                        }
                    }
                }
                ForgeVersionsInfo = ForgeVersionsInfo.OrderByDescending(x => x.GetGameVersion()).ToList();
            }
            return ForgeVersionsInfo;
        }

        public static List<WebVersionInfo> GetVersions(string source = Mojang.Version_Manifest, bool refresh = false)
        {
            if (WebVersionInfosCache == null|| refresh)
            {
                WebVersionInfosCache = RefreshVersion(source);
            }
            return WebVersionInfosCache;
        }

        public static bool TryGetVersions(out List<WebVersionInfo> infos, out string errorText, string source = Mojang.Version_Manifest, bool refresh = false)
        {
            try
            {
                infos = GetVersions(source, refresh);
                errorText = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                infos = null;
                errorText = ex.Message;
                return false;
            }
        }

        private static List<WebVersionInfo>? RefreshVersion(string versionManifestSource)
        {
            var res = new List<WebVersionInfo>();
            var request = HttpWebRequest.CreateHttp(versionManifestSource);
            using (var respon = request.GetResponse())
            {
                using (var strm = respon.GetResponseStream())
                {
                    using (var reader = new StreamReader(strm))
                    {

                        var jOb = JObject.Parse(reader.ReadToEnd());
                        foreach (var item in jOb["versions"])
                        {
                            res.Add(JsonConvert.DeserializeObject<WebVersionInfo>(item.ToString()));
                        }
                        return res;
                    }
                }
            }
        }
        private static List<OptifineFileInfo>? RefreshOptiFineFiles(string api)
        {
            var res = new List<OptifineFileInfo>();
            var request = HttpWebRequest.CreateHttp(api);
            using (var respon = request.GetResponse())
            {
                using (var strm = respon.GetResponseStream())
                {
                    using (var reader = new StreamReader(strm))
                    {
                        var jOb = JObject.Parse(reader.ReadToEnd());
                        foreach (var item in jOb["files"])
                        {
                            res.Add(JsonConvert.DeserializeObject<OptifineFileInfo>(item.ToString()));
                        }
                        return res;
                    }
                }
            }
        }
        public static List<OptifineFileInfo> GetOptiFineFiles(string api = DownloadSources.OPTIFINE_CN_API, bool refresh = false)
        {
            if (refresh || OptiFineFilesCache == null)
            {
                OptiFineFilesCache = RefreshOptiFineFiles(api);
            }
            return OptiFineFilesCache;
        }
    }
}
