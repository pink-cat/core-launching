﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreLaunching.DownloadAPIs.Forge
{

    public class ForgeFeaturedResult
    {
        public ThreeDatas Data { get; set; }
    }

    public class ThreeDatas
    {
        public ForgeModInfo[] Featured { get; set; }
        public ForgeModInfo[] Popular { get; set; }
        public ForgeModInfo[] RecentlyUpdated { get; set; }
    }

    public class ForgeFilesData
    {
        public ForgeFileInfo[] Data { get;set; }
        public Pagination Pagination { get; set; }
    }
}
