﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CoreLaunching.DownloadAPIs.Forge
{
    public class ForgeModPackManifest
    {
        public override string ToString()
        {
            return Name;
        }
        [JsonProperty("minecraft")]
        public ForgeModPackMinecraftInfo Minecraft { get; set; }
        [JsonProperty("manifestType")]
        public string ManifestType { get; set; }
        [JsonProperty("manifestVersion")]
        public int ManifestVersion { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("version")]
        public string Version { get; set; }
        [JsonProperty("author")]
        public string Author { get; set; }
        [JsonProperty("files")]
        public ForgeModFilePair[] Files { get; set; }
        [JsonProperty("overrides")]
        public string Overrides { get; set; }
    }

    public class ForgeModPackMinecraftInfo
    {
        [JsonProperty("version")]
        public string Version { get; set; }
        [JsonProperty("modLoaders")]
        public Modloader[] ModLoaders { get; set; }
    }

    public class Modloader
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("primary")]
        public bool Primary { get; set; }
    }

    public class ForgeModFilePair
    {
        [JsonProperty("projectID")]
        public int ProjectID { get; set; }
        [JsonProperty("fileID")]
        public int FileID { get; set; }
        [JsonProperty("required")]
        public bool Required { get; set; }
    }

    public class ForgeModPackArch:IDisposable
    {
        private bool disposedValue;

        public Stream BaseStream { get; set; }
        public ForgeModPackManifest Manifest { get; set; }
        public ForgeModPackArch(Stream stream)
        {
            BaseStream = stream;
            using (var manifestJStrm = ZipFile.GetSubFileDataStream("manifest.json",BaseStream))
            {
                using (var sr = new StreamReader(manifestJStrm))
                {
                    Manifest = JsonConvert.DeserializeObject<ForgeModPackManifest>(sr.ReadToEnd());
                }
            }
        }

        public void Override(string targetFolder, string overrideSign)
        {
            BaseStream.Position = 0;
            var ZipFile = new System.IO.Compression.ZipArchive(BaseStream);
            foreach (var item in ZipFile.Entries)
            {
                if (item.FullName.StartsWith(overrideSign))
                {
                    using (var file = item.Open())
                    {
                        var local = Path.Combine(targetFolder,item.FullName.Substring(overrideSign.Length+1,item.FullName.Length-1-overrideSign.Length).Replace("/","\\"));
                        Directory.CreateDirectory(Path.GetDirectoryName(local));
                        using (var fs = File.Create(local))
                        {
                            file.CopyTo(fs);
                        }
                    }
                }
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    BaseStream.Close();
                }
                BaseStream.Dispose();
                BaseStream = null;
                GC.Collect();
                // TODO: 释放未托管的资源(未托管的对象)并重写终结器
                // TODO: 将大型字段设置为 null
                disposedValue = true;
            }
        }

        // // TODO: 仅当“Dispose(bool disposing)”拥有用于释放未托管资源的代码时才替代终结器
        // ~ForgeModPackZipInfo()
        // {
        //     // 不要更改此代码。请将清理代码放入“Dispose(bool disposing)”方法中
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // 不要更改此代码。请将清理代码放入“Dispose(bool disposing)”方法中
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
