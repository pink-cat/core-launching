﻿using CoreLaunching.DownloadAPIs.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreLaunching.DownloadAPIs.Forge
{
    public static class ForgeInfoOrder
    {
        public static Version GetGameVersion(IEnumerable<string> str)
        {
            Version res = null;
            foreach (var item in str)
            {
                if (Version.TryParse(item, out var v))
                {
                    res = v;
                    break;
                }
            }
            return res;
        }
        public static IEnumerable<IModFileInfo> OrderByVersion(this IEnumerable<IModFileInfo> array, bool descending = true)
        {
            if (descending)
            {
                return array.ToList().OrderByDescending(x => GetGameVersion(x.Versions));
            }
            else
            {
                return array.ToList().OrderBy(x => GetGameVersion(x.Versions));
            }
        }

        public static ForgeFileInfo[] OrderByVersion(this ForgeFileInfo[] array, bool descending = true)
        {
            if (descending)
            {
                return array.ToList().OrderByDescending(x => GetSortableVersion(x.GameVersions)).ToArray();
            }
            else
            {

                return array.ToList().OrderBy(x => GetSortableVersion(x.GameVersions)).ToArray();
            }
        }

        private static Version GetSortableVersion(string[] strings)
        {
            var resu = new Version();
            foreach (var item in strings)
            {
                if (Version.TryParse(item, out var v))
                {
                    return v;
                }
            }
            return resu;
        }
        public static ForgeFileInfo[] GetDependencyDownloadInfos(this ForgeFileInfo info)
        {
            var details = info.GetDependencyDetails();
            var res = new ForgeFileInfo[details.Length];
            for (int i = 0; i < res.Length; i++)
            {
                var item = details[i];
                var files = item.LatestFilesIndexes;
                for (int j = 0; j < files.Length; j++)
                {
                    var file = files[j];
                    if (info.GameVersions.Contains(file.GameVersion) && info.GameVersions.Contains("Forge"))
                    {
                        if (file.ModLoader == ModLoaderType.Forge)
                        {
                            res[i] = info;
                            break;
                        }
                    }
                    else if (info.GameVersions.Contains(file.GameVersion) && info.GameVersions.Contains("Fabric"))
                    {

                        if (file.ModLoader == ModLoaderType.Fabric)
                        {
                            res[i] = info;
                            break;
                        }
                    }
                }
            }
            return res.ToArray();
        }
        public static ForgeModInfo[] GetDependencyDetails(this ForgeFileInfo info, RelationType relation = RelationType.RequiredDependency)
        {
            var array = info.Dependencies;
            var details = new List<ForgeModInfo>();
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i].RelationType == RelationType.RequiredDependency)
                {
                    details.Add(ForgeResourceFinder.FromModId(array[i].ModId));
                }
            }
            return details.ToArray();
        }

        public static ForgeModInfo[] OrderByName(this ForgeModInfo[] array, bool descending = false)
        {
            if (descending)
            {
                return array.ToList().OrderByDescending(x => x.Name).ToArray();
            }
            else
            {

                return array.ToList().OrderBy(x => x.Name).ToArray();
            }
        }

        public static ForgeModInfo[] OrderByDownloadCount(this ForgeModInfo[] array, bool descending = false)
        {
            if (descending)
            {
                return array.ToList().OrderByDescending(x => x.DownloadCount).ToArray();
            }
            else
            {
                return array.ToList().OrderBy(x => x.DownloadCount).ToArray();
            }
        }

    }
    public enum ModLoaderType
    {
        Any = 0,
        Forge = 1,
        Cauldron = 2,
        LitLoader = 3,
        Fabric = 4,
        Quilt = 5
    }

    public enum SortFiled
    {
        Featured = 1,
        Popularity = 2,
        LastUpdated = 3,
        Name = 4,
        Author = 5,
        TotalDownloads = 6,
        Category = 7,
        GameVersion = 8,
        EarlyAccess = 9
    }

    public class ForgeListResult : IResultPage
    {
        public ForgeModInfo[] Data { get; set; }
        public Pagination Pagination { get; set; }

        public IEnumerable<IModInfo> Contents => Data;

        public int TotalCount => (int)Math.Ceiling((double)Pagination.TotalCount / (double)Pagination.ResultCount);

        public int Current => Pagination.Index;

        public string KeyWords => _keywords;
        internal string _keywords;
        public IEnumerable<IModInfo> NextPage()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IModInfo> PreviousPage()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            if (Pagination == null)
            {
                return $"{Data.Length} 个结果";
            }
            else
            {
                return $"共{Pagination.TotalCount} 个结果，当前页面序号 {Pagination.Index} 总共 {Pagination.TotalCount / 50} 页面";
            }
        }
    }

    public class Pagination
    {
        public int Index { get; set; }
        public int PageSize { get; set; }
        public int ResultCount { get; set; }
        public int TotalCount { get; set; }
    }

    public class ForgeModInfo : IModInfo
    {
        #region Overrides
        public IEnumerable<string> GameVersions
        {
            get
            {
                var list = new string[0];
                foreach (var item in LatestFilesIndexes)
                {
                    list = list.Intersect(item.Versions).ToArray();
                }
                return list;
            }
        }

        public string IconSourceLink => Logo.Url;
        public string Description => Summary;
        public string DownloadCountsInfo => DownloadCountInChinese;

        public IEnumerable<string> Tags => Categories.Select(c => c.Name);

        public IEnumerable<IModFileInfo> GetDownloadLinks() => LatestFilesIndexes.OrderByVersion();
        #endregion

        #region Props

        [JsonIgnore]
        public string DownloadCountInChinese
        {
            get
            {
                if (DownloadCount > 100000000)
                {
                    var doub = (double)DownloadCount / 100000000.0;
                    return $"{Math.Round(doub, 3)} 亿";
                }
                else if (DownloadCount > 10000)
                {
                    var doub = (double)DownloadCount / 10000.0;
                    return $"{Math.Round(doub, 3)} 万";
                }
                else
                {
                    return $"{DownloadCount}";
                }
            }
        }
        public override string ToString()
        {
            return Name;
        }
        public int Id { get; set; }
        public int GameId { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public Links Links { get; set; }
        public string Summary { get; set; }
        public int Status { get; set; }
        public int DownloadCount { get; set; }
        public bool IsFeatured { get; set; }
        public int PrimaryCategoryId { get; set; }
        public Category[] Categories { get; set; }
        public int ClassId { get; set; }
        public Logo Logo { get; set; }
        public Screenshot[] Screenshots { get; set; }
        public int MainFileId { get; set; }
        public ForgeFileInfo[] LatestFiles { get; set; }
        public LatestFilesIndex[] LatestFilesIndexes { get; set; }
        public object[] LatestEarlyAccessFilesIndexes { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public DateTime DateReleased { get; set; }
        public bool? AllowModDistribution { get; set; }
        public int GamePopularityRank { get; set; }
        public bool IsAvailable { get; set; }
        public int ThumbsUpCount { get; set; }
        public IEnumerable<AuthorInfo> Authors { get; set; }

        public string GetLogoUrl()
        {
            if (Logo != null) return Logo.Url;
            else return "";
        }
        #endregion
    }

    public class Links
    {
        public string WebsiteUrl { get; set; }
        public string WikiUrl { get; set; }
        public string IssuesUrl { get; set; }
        public string SourceUrl { get; set; }
    }

    public class Logo
    {
        public int Id { get; set; }
        public int ModId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ThumbnailUrl { get; set; }
        public string Url { get; set; }
    }

    public class Category
    {
        public override string ToString()
        {
            return Name;
        }
        public int Id { get; set; }
        public int GameId { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string Url { get; set; }
        public string IconUrl { get; set; }
        public DateTime DateModified { get; set; }
        public bool IsClass { get; set; }
        public int ClassId { get; set; }
        public int ParentCategoryId { get; set; }
    }

    public class AuthorInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }

        public override string ToString()
        {
            return $"{Name}: {Url}";
        }
    }

    public class Screenshot
    {
        public int Id { get; set; }
        public int ModId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ThumbnailUrl { get; set; }
        public string Url { get; set; }
    }

    public class ForgeFileInfo : IModFileInfo
    {
        #region Overrides

        public IEnumerable<IModInfo> GetDependences() => this.GetDependencyDetails();

        public string Url => DownloadUrl;

        public string Sha1
        {
            get
            {
                var res = Hashes.Where(x => x.Algo == AlgoType.SHA1).ToArray();
                if (res.Length > 0)
                {
                    return res[0].Value;
                }
                else
                {
                    return "";
                }
            }
        }

        public long Size => FileLength;
        public IEnumerable<IModFileInfo> GetAllFileInfos()
        {
            var res = new List<IModFileInfo>
                {
                    this
                };
            var depen = GetDependences();
            if (depen.Count() > 0)
            {
                foreach (var dep in depen)
                {
                    var d = dep as ForgeModInfo;
                    foreach (var each in d.LatestFilesIndexes)
                    {
                        each.ModId = d.Id;
                        res.AddRange(each.GetAllFileInfos().ToArray());
                    }
                }
            }
            return res.Where((x, i) => res.FindIndex(z => z.Name == x.Name) == i);

        }

        public IEnumerable<string> Versions => GameVersions;

        public IEnumerable<string> SupportedLoaders => new string[1] { ModLoader.ToString() };

        public string Name => DisplayName;
        #endregion
        public override string ToString()
        {
            var v = "";
            for (int i = 0; i < GameVersions.Length; i++)
            {
                var item = GameVersions[i];
                if (i + 1 == GameVersions.Length)
                {
                    v += item;
                }
                else
                {
                    v += item;
                    v += ", ";
                }
            }
            return $"{DisplayName} ({v})";
        }

        public string ChineseName { get; set; }

        public string GetLocalName()
        {
            if (!string.IsNullOrEmpty(ChineseName))
            {
                return $"[{ChineseName}]{FileName}";
            }
            else
            {
                return FileName;
            }

        }

        public int Id { get; set; }
        public int GameId { get; set; }
        public int ModId { get; set; }
        public bool IsAvailable { get; set; }
        public string DisplayName { get; set; }
        public string FileName { get; set; }
        public FileReleaseType ReleaseType { get; set; }
        public int FileStatus { get; set; }
        public Hash[] Hashes { get; set; }
        public DateTime FileDate { get; set; }
        public long FileLength { get; set; }

        [JsonIgnore]
        public string ChineseDownloadCount
        {
            get
            {
                if (DownloadCount > 100000000)
                {
                    var doub = (double)DownloadCount / 100000000.0;
                    return $"{Math.Round(doub, 3)} 亿";
                }
                else if (DownloadCount > 10000)
                {
                    var doub = (double)DownloadCount / 10000.0;
                    return $"{Math.Round(doub, 3)} 万";
                }
                else
                {
                    return $"{DownloadCount}";
                }
            }
        }
        public long DownloadCount { get; set; }
        public string DownloadUrl { get; set; }
        public string[] GameVersions { get; set; }
        [JsonIgnore]
        public ModLoaderType ModLoader
        {
            get
            {
                if (GameVersions.Contains("Forge"))
                {
                    return ModLoaderType.Forge;
                }
                else if (GameVersions.Contains("Fabric"))
                {
                    return ModLoaderType.Fabric;
                }
                else { return ModLoaderType.Any; }
            }
        }
        public Sortablegameversion[] SortableGameVersions { get; set; }
        public Dependency[] Dependencies { get; set; }
        public int AlternateFileId { get; set; }
        public bool IsServerPack { get; set; }
        public long FileFingerprint { get; set; }
        public Module[] Modules { get; set; }
    }

    public enum FileReleaseType
    {
        Release = 1,

        Beta = 2,

        Alpha = 3
    }

    public class Dependency
    {
        public override string ToString()
        {
            return ModId.ToString();
        }
        public int ModId { get; set; }
        public RelationType RelationType { get; set; }
    }

    public enum RelationType
    {
        EmbeddedLibrary = 1,

        OptionalDependency = 2,

        RequiredDependency = 3,

        Tool = 4,

        Incompatible = 5,

        Include = 6
    }

    public class Hash
    {
        public string Value { get; set; }
        public AlgoType Algo { get; set; }
    }

    public enum AlgoType
    {
        SHA1 = 1,
        MD5 = 2
    }

    public class Sortablegameversion
    {
        public string GameVersionName { get; set; }
        public string GameVersionPadded { get; set; }
        public string GameVersion { get; set; }
        public DateTime GameVersionReleaseDate { get; set; }
        public int? GameVersionTypeId { get; set; }
    }

    public class Module
    {
        public string Name { get; set; }
        public long Fingerprint { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }

    public class LatestFilesIndex : IModFileInfo
    {
        #region Overrides
        private ForgeFileInfo _init;

        public ForgeFileInfo Detail => _init ?? ForgeResourceFinder.GetFromModIdAndFileId(ModId, FileId);

        public string Url => Detail.DownloadUrl;

        public string Sha1 => Detail.Sha1;

        public long Size => Detail.Size;

        public IEnumerable<IModFileInfo> GetAllFileInfos()
        {
            var res = new List<IModFileInfo>
                {
                    this
                };
            var depen = GetDependences();
            if (depen.Count() > 0)
            {
                foreach (var dep in depen)
                {
                    var d = dep as ForgeModInfo;
                    foreach (var each in d.LatestFilesIndexes.Where(x => x.GameVersion == GameVersion))
                    {
                        each.ModId = d.Id;
                        res.AddRange(each.GetAllFileInfos().ToArray());
                    }
                }
            }
            return res.Where((x, i) => res.FindIndex(z => z.Name == x.Name) == i);

        }

        public IEnumerable<string> Versions => new string[1] { GameVersion };

        public IEnumerable<string> SupportedLoaders => new string[1] { ModLoader.ToString() };

        public string Name => Filename;
        public IEnumerable<IModInfo> GetDependences() => ForgeResourceFinder.GetFromModIdAndFileId(ModId, FileId).GetDependencyDetails();
        #endregion
        public override string ToString()
        {
            return $"{Filename}: {FileId} ({GameVersion})";
        }

        public string GetLocalName()
        {
            throw new NotImplementedException();
        }

        public string GameVersion { get; set; }
        public int FileId { get; set; }
        public string Filename { get; set; }
        public int ReleaseType { get; set; }
        public int GameVersionTypeId { get; set; }
        public ModLoaderType ModLoader { get; set; }
        internal int ModId { get; set; }
    }

}
