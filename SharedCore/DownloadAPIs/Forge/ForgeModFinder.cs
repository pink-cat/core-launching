﻿using CoreLaunching.MicrosoftAuth;
using CoreLaunching.PinKcatDownloader;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;

namespace CoreLaunching.DownloadAPIs.Forge
{
    public class ForgeResourceFinder
    {
        private class SingleSlugFinder
        {
            public SingleSlugFinder(string slug)
            {
                _slug = slug;
            }
            public string _slug;
            public void Find()
            {
                 result = FindMinecraft(slug:_slug);
            }
            public ForgeListResult result;
        }

        private class SingleFileFinder
        {
            public MCFileInfo result;
            public int ProjectID;
            public int FileID;
            public string ModFolderPath;
            public event EventHandler<EventArgs> Finished;
            public SingleFileFinder(int modId,int fileId,string modFolderPath)
            {
                ProjectID = modId;
                FileID = fileId;
                ModFolderPath = modFolderPath;
            }

            public SingleFileFinder(ForgeModFilePair pair, string modFolderPath) :this(pair.ProjectID,pair.FileID,modFolderPath)
            {

            }

            public void Find()
            {
                bool succ = false;
                ForgeFileInfo info = null;
                while (!succ)
                {
                    try
                    {
                        info = GetFromModIdAndFileId(ProjectID, FileID);
                        succ = true;
                    }
                    catch (Exception ex)
                    {
                        Thread.Sleep(2000);
                    }
                }
                var native = info.DownloadUrl;
                if(string.IsNullOrEmpty(native))
                {
                    native= $"https://mediafilez.forgecdn.net/files/{FileID.ToString().Substring(0, 4)}/{FileID.ToString().Substring(4, FileID.ToString().Length - 4)}/{info.FileName}";
                }
                else
                {
                    native = LinkUtil.Redirect(info.DownloadUrl);    
                }
                var loacl = Path.Combine(ModFolderPath, Path.GetFileName(native));
                var sha1 = info.Sha1;
                var length = info.FileLength;
                var id = Path.GetFileName(native);
                result = new MCFileInfo(id, sha1, length, native, loacl);
                Finished.Invoke(this,null);
            }
        }
        public static IEnumerable<MCFileInfo> MutilGetFileUrlFromModIds(IEnumerable<ForgeModFilePair> pairs,string modsPath)
        {
            int chushu = 20;
            var res = new SingleFileFinder[pairs.Count()];
            var threads = new Thread[pairs.Count()];
            int running = 0;
            int max = 64;
            for (int i = 0; i < threads.Length; i++)
            {
                while (running+1>max)
                {
                    Thread.Sleep(100);
                }
                var resi = new SingleFileFinder(pairs.ToArray()[i], modsPath);
                resi.Finished += (_, _) =>
                {
                    running--;
                };
                res[i] = resi;
                threads[i] = new Thread(res[i].Find);
                threads[i].Start();
                running++;
                Thread.Sleep(100);
            }
            bool finished = false;
            while (!finished)
            {
                for (int i = 0; i < threads.Length; i++)
                {
                    if (threads[i].ThreadState != ThreadState.Stopped)
                    {
                        finished = false;
                        Thread.Sleep(100);
                        break;
                    }
                    finished = true;
                }
            }
            for (int i = 0; i < threads.Length; i++)
            {
                GC.SuppressFinalize(threads[i]);
                threads[i] = null;
            }
            GC.SuppressFinalize(threads);
            var _l = new List<MCFileInfo>();
            foreach (var item in res)
            {
                if (item.result != null)
                {
                    _l.Add(item.result);
                }
            }
            GC.SuppressFinalize(res);
            return _l;
        }

        public static IEnumerable<ForgeModInfo> MutilGetInfos(IEnumerable<string> slugs)
        {
            var res = new SingleSlugFinder[slugs.Count()];
            var threads = new Thread[slugs.Count()];
            for (int i = 0; i < threads.Length; i++)
            {
                res[i] = new SingleSlugFinder(slugs.ToArray()[i]);
                threads[i] = new Thread(res[i].Find);
                threads[i].Start();
                Thread.Sleep(100);
            }
            bool finished = false;
            while (!finished)
            {
                for (int i = 0; i < threads.Length; i++)
                {
                    if (threads[i].ThreadState != ThreadState.Stopped)
                    {
                        finished = false;
                        Thread.Sleep(100);
                        break;
                    }
                    finished = true;
                }
            }
            for (int i = 0; i < threads.Length; i++)
            {
                GC.SuppressFinalize(threads[i]);
                threads[i] = null;
            }
            GC.SuppressFinalize(threads);
            var _l = new List<ForgeModInfo>();
            foreach (var item in res)
            {
                if(item.result != null)
                {
                    _l.Add(item.result.Data.FirstOrDefault());
                }
            }
            GC.SuppressFinalize(res);
            return _l;
        }

        public const string BaseUrl = "https://api.curseforge.com";
        public static ForgeListResult FromGame(string gameId = "432")
        {
            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Get, $"{BaseUrl}/v1/mods/search?gameId={gameId}"))
                {
                    request.Headers.Add("x-api-key", $"{CLForgeTokens.StillMSFAN}");
                    var response = client.Send(request);
                    response.EnsureSuccessStatusCode();
                    using (var stm = response.Content.ReadAsStream())
                    {
                        using (var r = new StreamReader(stm))
                        {
                            var res = JsonConvert.DeserializeObject<ForgeListResult>(r.ReadToEnd());
                            res._keywords = "";
                            foreach (var item in res.Data)
                            {
                                foreach (var ind in item.LatestFilesIndexes)
                                {
                                    ind.ModId = item.Id;
                                }
                            }
                            return res;
                        }
                    }
                }
            }
        }
        public static bool FromGame(out ForgeListResult content,out string errorInfo, string gameId = "432")
        {
            try
            {
                content = FromGame(gameId);
                errorInfo = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                content = null;
                errorInfo = ex.Message;
                return false;
            }
        }
        public static bool FindMinecraft(out ForgeListResult result, out string e, string keyWords = "",string slug ="",int categoryId=0, string minecraftVersion = "", int modLoaderType = (int)ModLoaderType.Any)
        {
            e = null;
            try
            {
                result = FindMinecraft(keyWords,slug, categoryId, minecraftVersion,modLoaderType);
                return true;
            }
            catch (Exception ex)
            {
                e = ex.Message;
                result = null;
                return false;
            }
        }
        public static ForgeListResult FindMinecraft(string keyWords = "", string slug = "", int classId = 0, string minecraftVersion = "",int modLoaderType= (int)ModLoaderType.Any)
        {
            using (var client = new HttpClient())
            {
                var url = $"{BaseUrl}/v1/mods/search?gameId=432";
                if (modLoaderType!=(int)ModLoaderType.Any)
                {
                    url += $"&modLoaderType={modLoaderType}";
                }
                if (!string.IsNullOrEmpty(keyWords))
                {
                    url += $"&searchFilter={keyWords}";
                }
                if (!string.IsNullOrEmpty(slug))
                {
                    url += $"&slug={slug}";
                }
                if (!string.IsNullOrEmpty(minecraftVersion))
                {
                    url += $"&gameVersion={minecraftVersion}";
                }
                if (classId != 0)
                {
                    url += $"&classId={classId}";
                }
                using (var request = new HttpRequestMessage(HttpMethod.Get, url))
                {
                    request.Headers.Add("x-api-key", $"{CLForgeTokens.StillMSFAN}");
                    var response = client.Send(request);
                    response.EnsureSuccessStatusCode();
                    using (var stm = response.Content.ReadAsStream())
                    {
                        using (var r = new StreamReader(stm))
                        {
                            var res = JsonConvert.DeserializeObject<ForgeListResult>(r.ReadToEnd());
                            res._keywords = keyWords;
                            foreach (var item in res.Data)
                            {
                                foreach (var ind in item.LatestFilesIndexes)
                                {
                                    ind.ModId = item.Id;
                                }
                            }
                            res.Data = res.Data.OrderByDescending(x => x.DownloadCount).ToArray();
                            return res;
                        }
                    }
                }
            }
        }
        public static ForgeListResult FromName(string keyWords,string gameId = "432")
        {
            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Get, $"{BaseUrl}/v1/mods/search?gameId={gameId}&searchFilter={keyWords}"))
                {
                    request.Headers.Add("x-api-key", $"{CLForgeTokens.StillMSFAN}");
                    var response = client.Send(request);
                    response.EnsureSuccessStatusCode();
                    using (var stm = response.Content.ReadAsStream())
                    {
                        using (var r = new StreamReader(stm))
                        {
                            var res = JsonConvert.DeserializeObject<ForgeListResult>(r.ReadToEnd());
                            res._keywords = keyWords; 
                            foreach (var item in res.Data)
                            {
                                foreach (var ind in item.LatestFilesIndexes)
                                {
                                    ind.ModId = item.Id;
                                }
                            }
                            res.Data = res.Data.OrderByDescending(x => x.DownloadCountsInfo).ToArray();
                            return res;
                        }
                    }
                }
            }   
        }
        
        public static bool GetFeatured(out ForgeFeaturedResult result,out string errorInfo,string gameId = "432")
        {
            try
            {
                errorInfo = "";
                result = GetFeatured(gameId);
                return true;
            }
            catch (Exception e)
            {
                errorInfo = e.Message;
                result = null;
                return false;
                throw;
            }
        }
        public static ForgeFeaturedResult GetFeatured(string gameId="432")
        {
            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Post, $"{BaseUrl}/v1/mods/featured"))
                {
                    request.Headers.Add("x-api-key", CLForgeTokens.StillMSFAN);
                    request.Headers.Add("Accept", "application/json");
                    var content = new StringContent($"{{\r\n  \"gameId\": {gameId},\r\n  \"excludedModIds\": [\r\n    0\r\n  ],\r\n  \"gameVersionTypeId\": null\r\n}}", null, "application/json");
                    request.Content = content;
                    var response = client.Send(request);
                    response.EnsureSuccessStatusCode();
                    using (var stm = response.Content.ReadAsStream())
                    {
                        using (var r = new StreamReader(stm))
                        {
#if DEBUG
                            var cont = r.ReadToEnd();
                            r.BaseStream.Position = 0;
#endif
                            var res = JsonConvert.DeserializeObject<ForgeFeaturedResult>(r.ReadToEnd());
                            foreach (var item in res.Data.Popular)
                            {
                                foreach (var ind in item.LatestFilesIndexes)
                                {
                                    ind.ModId = item.Id;
                                }
                            }
                            return res;
                        }
                    }
                }
            }
        }
        public static ForgeFilesData GetFilesFromModId(int modId, string version = null)
        {
            using (var client = new HttpClient())
            {
                var url = $"{BaseUrl}/v1/mods/{modId}/files";
                if (!string.IsNullOrEmpty(version))
                {
                    url = $"{BaseUrl}/v1/mods/{modId}/files?gameVersion={version}";
                }
                using (var request = new HttpRequestMessage(HttpMethod.Get, url))
                {
                    request.Headers.Add("x-api-key", CLForgeTokens.StillMSFAN);
                    request.Headers.Add("Accept", "application/json");
                    var response = client.Send(request);
                    response.EnsureSuccessStatusCode();
                    using (var stm = response.Content.ReadAsStream())
                    {
                        using (var r = new StreamReader(stm))
                        {
                            return JsonConvert.DeserializeObject<ForgeFilesData>(r.ReadToEnd());
                        }
                    }
                }
            }
        }
        public static ForgeModInfo FromModId(int modId)
        {
            using (var client = new HttpClient())
            {
                var url = $"{BaseUrl}/v1/mods/{modId}";
                using (var request = new HttpRequestMessage(HttpMethod.Get, url))
                {
                    request.Headers.Add("x-api-key", CLForgeTokens.StillMSFAN);
                    request.Headers.Add("Accept", "application/json");
                    var response = client.Send(request);
                    response.EnsureSuccessStatusCode();
                    using (var stm = response.Content.ReadAsStream())
                    {
                        using (var r = new StreamReader(stm))
                        {
#if DEBUG
                            var str = r.ReadToEnd(); r.BaseStream.Position = 0;
#endif
                            var jOb = JObject.Parse(r.ReadToEnd());
                            return JsonConvert.DeserializeObject<ForgeModInfo>(jOb["data"].ToString());
                        }
                    }
                }
            }
        }
        public static ForgeFileInfo GetFromModIdAndFileId(int modId,int fileId)
        {
            using (var client = new HttpClient())
            {
                var url = $"{BaseUrl}/v1/mods/{modId}/files/{fileId}";
                using (var request = new HttpRequestMessage(HttpMethod.Get, url))
                {
                    request.Headers.Add("x-api-key", CLForgeTokens.StillMSFAN);
                    request.Headers.Add("Accept", "application/json");
                    var response = client.Send(request);
                    response.EnsureSuccessStatusCode();
                    using (var stm = response.Content.ReadAsStream())
                    {
                        using (var r = new StreamReader(stm))
                        {
                            var jOb = JObject.Parse(r.ReadToEnd());
                            var res = JsonConvert.DeserializeObject<ForgeFileInfo>(jOb["data"].ToString());
                            return res;
                        }
                    }
                }
            }
        }
    }

    internal class CLForgeTokens
    {
        internal const string StillMSFAN = "$2a$10$WLgQmnTUtJhSU1ERD26PpukdHe0Hfk18AhmJoymM5mgzS2vzvQiCy";
    }
}
