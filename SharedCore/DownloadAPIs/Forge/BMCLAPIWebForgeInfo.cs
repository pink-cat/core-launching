﻿using CoreLaunching.DownloadAPIs.Universal;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;

namespace CoreLaunching.DownloadAPIs.Forge
{
    public class ForgeVersionInfoInstance : IForgeVersionInfo
    {
        public override string ToString() => Name;
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("gameVersion")]
        public string GameVersionString { get; set; }
        [JsonProperty("latest")]
        public bool Latest { get; set; }
        [JsonProperty("recommended")]
        public bool Recommended { get; set; }
        [JsonProperty("dateModified")]
        public DateTime DateModified { get; set; }

        public Version GetGameVersion()
        {
            if (GameVersionString.Contains("pre"))
            {
                var majs = GameVersionString.Split('.');
                var maj = "";
                foreach (var item in majs)
                {
                    maj += item+".";
                    if (item.Contains('-'))
                    {
                        break;
                    }
                }
                return new Version(maj);
            }
            else
            {
                return Version.Parse(GameVersionString);
            }
        }

        public string GetInstallJarUrl() => $"https://maven.minecraftforge.net/net/minecraftforge/forge/{GameVersionString}-{Name.Replace($"forge-","")}/forge-{GameVersionString}-{Name.Replace($"forge-", "")}-installer.jar";
    }
    public interface IBaseModLoaderInfo
    {
        public string Name { get; set; }
        public string GameVersionString { get; set; }
        public Version GetGameVersion();
        public bool Latest { get; set; }
        public bool Recommended { get; set; }
        public DateTime DateModified { get; set; }
    }
    public interface IForgeVersionInfo:IBaseModLoaderInfo
    {
        public string GetInstallJarUrl();
    }
    public class WebForgeInfo:IForgeVersionInfo
    {
        public override string ToString()
        {
            return $"{Version} {Mcversion} {Modified}";
        }

        public string GetInstallJarUrl() => DownloadSources.BMCLAPI_Forge_Libraries.Replace("/maven/",string.Empty) + LinkUtil.Redirect($"https://bmclapi2.bangbang93.com/forge/download/{Build}");

        public Version GetGameVersion()
        {
            if (GameVersionString.Contains("pre"))
            {
                var majs = GameVersionString.Split('.');
                var maj = "";
                foreach (var item in majs)
                {
                    if (item.Contains("pre"))
                    {
                        var ite = item.Replace('_', '-');
                        var rev = "";
                        foreach (var item2 in ite)
                        {
                            if (item2 == '-')
                            {
                                break;
                            }
                            rev += item2;
                        }
                        maj += (System.Convert.ToInt32(rev) - 1).ToString();
                        maj += "." + ite.Last();
                        break;
                    }
                    else
                    {
                        maj += item + ".";
                    }
                }
                return System.Version.Parse(maj);
            }
            else
            {
                return System.Version.Parse(GameVersionString);
            }
        }

        [JsonProperty("_id")]
        public string ID { get; set; }
        [JsonProperty("__v")]
        public int V { get; set; }
        [JsonProperty("branch")]
        public string Branch { get; set; }
        [JsonProperty("build")]
        public string Build { get; set; }
        [JsonProperty("files")]
        public ForgeFile[] Files { get; set; }
        [JsonProperty("mcversion")]
        public string Mcversion { get; set; }
        [JsonProperty("modified")]
        public DateTime Modified { get; set; }
        [JsonProperty("version")]
        public string Version { get; set; }
        public string Name { get => Version; set { } }
        public string GameVersionString { get => Mcversion; set { } }
        public bool Latest { get => BMCLForgeDownloadAPI.GetRcommendedInfos().Where(x=>x.ID==this.ID).Count()>0; set => throw new NotImplementedException(); }
        public bool Recommended { get => BMCLForgeDownloadAPI.GetRcommendedInfos().Where(x => x.ID == this.ID).Count() > 0; set => throw new NotImplementedException(); }
        public DateTime DateModified { get=>Modified; set { } }
    }

    public class ForgeFile
    {
        [JsonProperty("format")]
        public string Format { get; set; }
        [JsonProperty("category")]
        public string Category { get; set; }
        [JsonProperty("hash")]
        public string Hash { get; set; }
        public override string ToString()
        {
            return $"{Format}:{Category}";
        }
    }

}
