﻿using CoreLaunching.PinKcatDownloader;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text;

namespace CoreLaunching.DownloadAPIs.Forge
{
    public class ForgeModPackInstaller
    {
        public static void Install(string packPath, string dotMCPath, bool useRelative = true,string versionPath="",ParseType parseType = ParseType.FilePath)
        {
            Directory.CreateDirectory(dotMCPath);
            ForgeModPackArch zip = null;
            string modsPath = "";
            if (parseType == ParseType.NativeUrl)
            {
                
            }
            else if (parseType == ParseType.FilePath)
            {
                var fs = File.OpenRead(packPath);
                zip = new(fs);
            }
            else if (parseType==ParseType.Json)
            {
                throw new InvalidOperationException("不是？你确定是个Json？");
            }
            if (string.IsNullOrEmpty(versionPath))
            {
                versionPath = Path.Combine(dotMCPath,zip.Manifest.Name);
            }
            if (useRelative)
            {
                modsPath = Path.Combine(versionPath,"mods");
            }
            if(useRelative)
            {
                dotMCPath = versionPath;
                Directory.CreateDirectory(dotMCPath);
            }
            Directory.CreateDirectory(modsPath);
            Directory.CreateDirectory(versionPath);

            //TODO 改为多线程版本。
            //TODO 记得查看其他待办事项。
#if true
            Console.WriteLine(DateTime.Now);
            var infos = ForgeResourceFinder.MutilGetFileUrlFromModIds(zip.Manifest.Files, modsPath);
            //var proms = new ProcessManager(infos); 
            Console.WriteLine(DateTime.Now);
#endif
            zip.Override(dotMCPath,zip.Manifest.Overrides);
            zip.Dispose();

            //TODO 安装游戏本体
        }
    }
}
