﻿using CoreLaunching.JsonTemplates;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace CoreLaunching.DownloadAPIs
{
    public static class Mojang
    {
        public const string Version_Manifest = "https://launchermeta.mojang.com/mc/game/version_manifest.json";
        private static List<WebVersionInfo> WebVersionInfos;
        [Obsolete("请使用 VersionsManager.GetVersion()")]
        public static List<WebVersionInfo> GetVersion() => WebVersionInfos ?? RefreshVersion();

        public static List<WebVersionInfo> RefreshVersion()
        {
            try
            {
                var res = new List<WebVersionInfo>();
                var request = HttpWebRequest.CreateHttp(Version_Manifest);
                try
                {
                    using (var respon = request.GetResponse())
                    {
                        using (var strm = respon.GetResponseStream())
                        {
                            using (var reader = new StreamReader(strm))
                            {

                                var jOb = JObject.Parse(reader.ReadToEnd());
                                foreach (var item in jOb["versions"])
                                {
                                    res.Add(JsonConvert.DeserializeObject<WebVersionInfo>(item.ToString()));
                                }
                                return res;
                            }
                        }
                    }
                }
                catch
                {
                    return new();
                }
            }
            catch
            {
                return null;
            }
        }
    }
}
