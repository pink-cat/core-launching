﻿using CoreLaunching.DownloadAPIs.Forge;
using System.Collections.Generic;

namespace CoreLaunching.DownloadAPIs.Interfaces
{
    public interface IResultPage
    {
        public IEnumerable<IModInfo> Contents { get; }
        public IEnumerable<IModInfo> NextPage();
        public IEnumerable<IModInfo> PreviousPage();
        public int TotalCount { get; }
        public int Current { get; }
        public string KeyWords { get; }
    }

    public interface IModInfo
    {
        public string Name { get; }
        public string Description { get; }
        public string DownloadCountsInfo { get; }
        public int DownloadCount { get; }
        public IEnumerable<string> Tags { get; }
        public IEnumerable<IModFileInfo> GetDownloadLinks();
        public string Slug { get; }
        public string IconSourceLink { get; }
        public IEnumerable<string> GameVersions { get; }
        IEnumerable<AuthorInfo> Authors { get; set; }
        string GetLogoUrl();
    }

    public interface IModFileInfo
    {
        public string GetLocalName();
        public IEnumerable<IModInfo> GetDependences();
        public string Url { get; }
        public string Sha1 { get; }
        public long Size { get; }
        public IEnumerable<IModFileInfo> GetAllFileInfos();
        public IEnumerable<string> Versions { get; }
        public IEnumerable<string> SupportedLoaders { get; }
        public string Name { get; }
        public ModLoaderType ModLoader { get; }
    }

}
