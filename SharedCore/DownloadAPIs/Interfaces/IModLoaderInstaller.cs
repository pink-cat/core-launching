using System.ComponentModel;

namespace CoreLaunching.DownloadAPIs.Interfaces
{
    /// <summary>
    /// ModLoader 安装器接口
    /// </summary>
    public interface IModLoaderInstaller:IDisposable,IShowProgress
    {
        /// <summary>
        /// 缓存清单， Dispose 时候清理。
        /// </summary>
        /// <returns>清单</returns>
        public IEnumerable<FileSystemInfo> Caches();
        /// <summary>
        /// 安装。
        /// </summary>
        public void Install();
        /// <summary>
        /// 安装（异步）。
        /// </summary>
        /// <returns>任务</returns>
        public Task InstallAsync();
        /// <summary>
        /// 安装器的状态。
        /// </summary>
        public InstallerStatuEnum InstallerStatu { get; set; }
        /// <summary>
        /// 输出事件。
        /// </summary>
        public event EventHandler<string> InfoOutput;
    }
    /// <summary>
    /// 展示进度接口。
    /// </summary>
    public interface IShowProgress:INotifyPropertyChanging
    {
        /// <summary>
        /// 当前步骤数。
        /// </summary>
        public int CurrectIndex { get; set; }
        /// <summary>
        /// 条目说明。
        /// </summary>
        public IEnumerable<string> Infos { get; set; }
        /// <summary>
        /// 总共进度（不要 *100 ）。
        /// </summary>
        public double Progress { get; set; }
        /// <summary>
        /// 接收到的字节数。
        /// </summary>
        public long BytesReceived { get; set; }
        /// <summary>
        /// 总共的字节数。
        /// </summary>
        public long TotalBytes { get; set; }
    }

    public enum InstallerStatuEnum
    {
        Installed,
        NotExecuted,
        Running,
        Failed
    }
}