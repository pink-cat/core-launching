﻿using CoreLaunching.DownloadAPIs.Forge;
using CoreLaunching.DownloadAPIs.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoreLaunching.DownloadAPIs.Modrinth
{
    public class ModrinthFileInfoCollection : List<ModrinthFileInfo>,IModInfo
    {
        #region Overrides 

        public string Name => throw new NotImplementedException();

        public string Description => throw new NotImplementedException();

        public string Author => throw new NotImplementedException();

        public string DownloadCountsInfo => throw new NotImplementedException();

        public IEnumerable<string> Tags => throw new NotImplementedException();

        public IEnumerable<IModFileInfo> GetDownloadLinks() => throw new NotImplementedException();

        public string Slug => throw new NotImplementedException();

        public string IconSourceLink => throw new NotImplementedException();

        public IEnumerable<string> GameVersions => throw new NotImplementedException();

        public IEnumerable<AuthorInfo> Authors { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public int DownloadCount => throw new NotImplementedException();

        #endregion
        #region Methods

        public string[] GetSupportedVersions()
        {
            var strs = new string[0];
            foreach (var item in this)
            {
                strs = strs.Union(item.Game_Versions).ToArray();
            }
            return strs;
        }

        List<Modrinth.File> files;
        internal IEnumerable<IModFileInfo> GetLinks()
        {
            if (files==null)
            {
                files = new();
                foreach (var item in this)
                {
                    foreach (var file in item.Files)
                    {
                        file._versions = item.Game_Versions;
                        file._loader = item.Loaders;
                        files.Add(file);
                    }
                }
            }
            return files;
        }

        public string GetLogoUrl()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
    public class ModrinthFileInfo
    {
        #region MyRegion

        public override string ToString()
        {
            return Name;
        }

        

        public IEnumerable<IModFileInfo> GetAllFileInfos()
        {
            throw new NotImplementedException();
        }

        public string Id { get; set; }
        public string Project_id { get; set; }
        public string Author_id { get; set; }
        public bool Featured { get; set; }
        public string Name { get; set; }
        public string Version_number { get; set; }
        public string Changelog { get; set; }
        public object Changelog_url { get; set; }
        public DateTime Date_published { get; set; }
        public int Downloads { get; set; }
        public string Version_type { get; set; }
        public string Status { get; set; }
        public object Requested_status { get; set; }
        public File[] Files { get; set; }
        public object[] Dependencies { get; set; }
        public string[] Game_Versions { get; set; }
        public string[] Loaders { get; set; }

        #endregion

    }

    public class File:IModFileInfo
    {
        public Hashes Hashes { get; set; }
        public string Url { get; set; }
        public string Filename { get; set; }
        public bool Primary { get; set; }
        public long Size { get; set; }
        public object File_type { get; set; }

        public string Sha1 => Hashes.Sha1;

        internal IEnumerable<string>? _versions;
        public IEnumerable<string> Versions => _versions;

        internal IEnumerable<string> _loader;
        public IEnumerable<string> SupportedLoaders => _loader;
        public string Name => Filename;

        public ModLoaderType ModLoader => throw new NotImplementedException();

        public IEnumerable<IModFileInfo> GetAllFileInfos()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IModInfo> GetDependences()
        {
            throw new NotImplementedException();
        }

        public string GetLocalName()
        {
            throw new NotImplementedException();
        }
    }

    public class Hashes
    {
        public string Sha512 { get; set; }
        public string Sha1 { get; set; }
    }
}
