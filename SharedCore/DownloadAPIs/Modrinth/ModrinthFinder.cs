﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

namespace CoreLaunching.DownloadAPIs.Modrinth
{
    public static class ModrinthFinder
    {
        public static ModrinthSearchResult Search(string grave = null)
        {
            var url = "";
            if (!string.IsNullOrEmpty(grave))
            {
                grave = grave.Replace(" ", "");
                url = $"{API_Base}/search?query={grave}";
            }
            else
            {
                url = $"{API_Base}/search";
            }
            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Get, url))
                {
                    using (var response = client.Send(request))
                    {
                        using (var str = response.Content.ReadAsStream())
                        {
                            response.EnsureSuccessStatusCode();
                            str.Position = 0;
                            using (var sr = new StreamReader(str))
                            {
                                var res = JsonConvert.DeserializeObject<ModrinthSearchResult>(sr.ReadToEnd());
                                res._keyWords = grave;
                                res.Contents.OrderByDescending(x=>x.DownloadCountsInfo);
                                return res;
                            }
                        }
                    }
                }
            }
        }
        public static ModrinthFileInfoCollection GetVersion(string idOrSlug)
        {
            var url = $"https://api.modrinth.com/v2/project/{idOrSlug}/version";
            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Get, url))
                {
                    using (var response = client.Send(request))
                    {
                        using (var str = response.Content.ReadAsStream())
                        {
                            response.EnsureSuccessStatusCode();
                            str.Position = 0;
                            using (var sr = new StreamReader(str))
                            {
                                var res = JsonConvert.DeserializeObject<ModrinthFileInfoCollection>(sr.ReadToEnd());
                                res.OrderByDescending(x => x.Game_Versions);
                                return res;
                            }
                        }
                    }
                }
            }
        }

        public static ProjectResult GetProjectInfo(string idOrSlug)
        {
            var url = $"https://api.modrinth.com/v2/project/{idOrSlug}";
            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Get, url))
                {
                    using (var response = client.Send(request))
                    {
                        using (var str = response.Content.ReadAsStream())
                        {
                            response.EnsureSuccessStatusCode();
                            str.Position = 0;
                            using (var sr = new StreamReader(str))
                            {
                                var strs = sr.ReadToEnd();
                                var res = JsonConvert.DeserializeObject<ProjectResult>(strs);
                                return res;
                            }
                        }
                    }
                }
            }
        }

        public const string API_Base = "https://api.modrinth.com/v2";
    }
}
