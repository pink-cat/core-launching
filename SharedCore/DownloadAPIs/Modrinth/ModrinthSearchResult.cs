﻿using CoreLaunching.DownloadAPIs.Forge;
using CoreLaunching.DownloadAPIs.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CoreLaunching.DownloadAPIs.Modrinth
{

    public class Hit : IModInfo
    {
        #region Overrides
        public IEnumerable<string> GameVersions => JVersions;
        public IEnumerable<AuthorInfo> Authors { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override string ToString()
        {
            return Name;
        }
        public string Name => Title;

        public string DownloadCountsInfo
        {
            get
            {
                if (Downloads > 100000000)
                {
                    var doub = (double)Downloads / 100000000.0;
                    return $"{Math.Round(doub, 3)} 亿";
                }
                else if (Downloads > 10000)
                {
                    var doub = (double)Downloads / 10000.0;
                    return $"{Math.Round(doub, 3)} 万";
                }
                else
                {
                    return $"{Downloads}";
                }
            }
        }

        public IEnumerable<string> Tags => Categories;

        public IEnumerable<IModFileInfo> GetDownloadLinks() => throw new NotImplementedException();

        public string GetLogoUrl()
        {
            throw new NotImplementedException();
        }

        public string IconSourceLink => Icon_url;
        #endregion

        #region Props
        public string Project_id { get; set; }
        public string Project_type { get; set; }
        public string Slug { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string[] Categories { get; set; }
        public string[] Display_categories { get; set; }
        [JsonProperty("versions")]
        public string[] JVersions { get; set; }
        public int Downloads { get; set; }
        public int Follows { get; set; }
        public string Icon_url { get; set; }
        public DateTime Date_created { get; set; }
        public DateTime Date_modified { get; set; }
        public string Latest_version { get; set; }
        public string License { get; set; }
        public string Client_side { get; set; }
        public string Server_side { get; set; }
        public string[] Gallery { get; set; }
        public string Featured_gallery { get; set; }
        public int? Color { get; set; }

        public int DownloadCount => throw new NotImplementedException();
        #endregion
    }

    public class ModrinthSearchResult : IResultPage
    {
        #region Overrides

        public IEnumerable<IModInfo> Contents => Hits;

        public int TotalCount => (int)Math.Ceiling(Total_hits / (double)Limit);

        public int Current => (int)Math.Truncate(d: Offset / (double)Limit);
        internal string _keyWords = "";

        public string KeyWords => _keyWords;

        public IEnumerable<IModInfo> NextPage()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IModInfo> PreviousPage()
        {
            throw new NotImplementedException();
        }
        #endregion

        public Hit[] Hits { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
        public int Total_hits { get; set; }
    }
}
