﻿using CoreLaunching.DownloadAPIs.Forge;
using CoreLaunching.DownloadAPIs.Interfaces;
using System;
using System.Collections.Generic;

namespace CoreLaunching.DownloadAPIs.Modrinth
{

    public class ProjectResult : IModInfo
    {
        #region Prop
        public string Id { get; set; }
        public string Slug { get; set; }
        public string Project_type { get; set; }
        public string Team { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        public object Body_url { get; set; }
        public DateTime Published { get; set; }
        public DateTime Updated { get; set; }
        public DateTime Approved { get; set; }
        public object Queued { get; set; }
        public string Status { get; set; }
        public object Requested_status { get; set; }
        public object Moderator_message { get; set; }
        public License License { get; set; }
        public string Client_side { get; set; }
        public string Server_side { get; set; }
        public int Downloads { get; set; }
        public int Followers { get; set; }
        public string[] Categories { get; set; }
        public object[] Additional_categories { get; set; }
        public string[] Game_versions { get; set; }
        public string[] Loaders { get; set; }
        public string[] Versions { get; set; }
        public string Icon_url { get; set; }
        public string Issues_url { get; set; }
        public string Source_url { get; set; }
        public object Wiki_url { get; set; }
        public string Discord_url { get; set; }
        public Donation_Urls[] Donation_urls { get; set; }
        public object[] Gallery { get; set; }
        public object Flame_anvil_project { get; set; }
        public object Flame_anvil_user { get; set; }
        public int Color { get; set; }

        #endregion
        #region Overrides
        public string Name => Title;

        public string Author => this.Team;

        public string DownloadCountsInfo
        {

            get
            {
                if (Downloads > 100000000)
                {
                    var doub = (double)Downloads / 100000000.0;
                    return $"{Math.Round(doub, 3)} 亿";
                }
                else if (Downloads > 10000)
                {
                    var doub = (double)Downloads / 10000.0;
                    return $"{Math.Round(doub, 3)} 万";
                }
                else
                {
                    return $"{Downloads}";
                }
            }
        }

        public IEnumerable<string> Tags => Categories;

        public IEnumerable<IModFileInfo> GetDownloadLinks()
        {
            var d = ModrinthFinder.GetVersion(Id);
            return d.GetLinks();

        }

        public string GetLogoUrl()
        {
            throw new NotImplementedException();
        }

        public string IconSourceLink => Icon_url;

        public IEnumerable<string> GameVersions => Game_versions;

        public IEnumerable<AuthorInfo> Authors { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public int DownloadCount => throw new NotImplementedException();
        #endregion
    }

    public class License
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public object Url { get; set; }
    }

    public class Donation_Urls
    {
        public string Id { get; set; }
        public string Platform { get; set; }
        public string Url { get; set; }
    }

}
